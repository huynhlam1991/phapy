using System.Collections.Generic;
using Abp.Application.Services.Dto;
using TranLe.PhapY.Editions.Dto;

namespace TranLe.PhapY.MultiTenancy.Dto
{
    public class GetTenantFeaturesForEditOutput
    {
        public List<NameValueDto> FeatureValues { get; set; }

        public List<FlatFeatureDto> Features { get; set; }
    }
}