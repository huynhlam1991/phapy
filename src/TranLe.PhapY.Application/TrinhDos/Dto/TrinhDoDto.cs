﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System.ComponentModel.DataAnnotations;

namespace TranLe.PhapY.TrinhDos.Dto
{
    [AutoMapTo(typeof(Models.TrinhDo))]
    public class TrinhDoDto : EntityDto
    {
        [Required(ErrorMessage = "Nhập tên trình độ")]
        [StringLength(100, ErrorMessage = "Số ký tự tối đa là 100")]
        public string TenTrinhDo { get; set; }
        [StringLength(200, ErrorMessage = "Số ký tự tối đa là 200")]
        public string DienGiai { get; set; }
    }
}
