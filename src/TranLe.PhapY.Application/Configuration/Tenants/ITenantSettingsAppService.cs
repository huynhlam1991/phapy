﻿using System.Threading.Tasks;
using Abp.Application.Services;
using TranLe.PhapY.Configuration.Tenants.Dto;

namespace TranLe.PhapY.Configuration.Tenants
{
    public interface ITenantSettingsAppService : IApplicationService
    {
        Task<TenantSettingsEditDto> GetAllSettings();

        Task UpdateAllSettings(TenantSettingsEditDto input);

        Task ClearLogo();

        Task ClearCustomCss();
    }
}
