﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using TranLe.PhapY.Authorization.Users;

namespace TranLe.PhapY.Configuration.Host.Dto
{
    public class SendTestEmailInput
    {
        [Required]
        [MaxLength(User.MaxEmailAddressLength)]
        public string EmailAddress { get; set; }
    }
}