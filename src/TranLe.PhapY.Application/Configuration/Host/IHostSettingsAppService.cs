﻿using System.Threading.Tasks;
using Abp.Application.Services;
using TranLe.PhapY.Configuration.Host.Dto;

namespace TranLe.PhapY.Configuration.Host
{
    public interface IHostSettingsAppService : IApplicationService
    {
        Task<HostSettingsEditDto> GetAllSettings();

        Task UpdateAllSettings(HostSettingsEditDto input);

        Task SendTestEmail(SendTestEmailInput input);
    }
}
