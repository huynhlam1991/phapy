﻿using Abp.Authorization;
using Abp.Runtime.Session;
using System.Threading.Tasks;
using TranLe.PhapY.Configuration.Dto;

namespace TranLe.PhapY.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService: PhapYAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
