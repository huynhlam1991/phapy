﻿using System.Threading.Tasks;
using Abp.Application.Services;
using TranLe.PhapY.Configuration.Dto;

namespace TranLe.PhapY.Configuration
{
    public interface IConfigurationAppService : IApplicationService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}