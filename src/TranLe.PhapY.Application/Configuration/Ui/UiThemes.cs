﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranLe.PhapY.Configuration.Ui
{
    public static class UiThemes
    {
        public static List<UiThemeInfo> All { get; }
        static UiThemes()
        {
            All = new List<UiThemeInfo>
            {
                new UiThemeInfo("BrightGray", "BrightGray"),
                new UiThemeInfo("SanMarino", "SanMarino"),
                new UiThemeInfo("OrangePeel", "OrangePeel"),
                new UiThemeInfo("Green", "Green"),
                new UiThemeInfo("Pomegranate", "Pomegranate")
            };
        }
    }
}
