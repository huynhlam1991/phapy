﻿using Abp.Application.Services;
using TranLe.PhapY.Tenants.Dashboard.Dto;

namespace TranLe.PhapY.Tenants.Dashboard
{
    public interface ITenantDashboardAppService : IApplicationService
    {
        GetMemberActivityOutput GetMemberActivity();
    }
}
