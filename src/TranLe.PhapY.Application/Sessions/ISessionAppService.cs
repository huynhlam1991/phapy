﻿using System.Threading.Tasks;
using Abp.Application.Services;
using TranLe.PhapY.Sessions.Dto;

namespace TranLe.PhapY.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
