﻿using System.Collections.Generic;
using TranLe.PhapY.Auditing.Dto;
using TranLe.PhapY.Dto;

namespace TranLe.PhapY.Auditing.Exporting
{
    public interface IAuditLogListExcelExporter
    {
        FileDto ExportToFile(List<AuditLogListDto> auditLogListDtos);
    }
}
