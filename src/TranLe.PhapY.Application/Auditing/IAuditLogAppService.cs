using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TranLe.PhapY.Auditing.Dto;
using TranLe.PhapY.Dto;

namespace TranLe.PhapY.Auditing
{
    public interface IAuditLogAppService : IApplicationService
    {
        Task<PagedResultDto<AuditLogListDto>> GetAuditLogs(GetAuditLogsInput input);

        Task<FileDto> GetAuditLogsToExcel(GetAuditLogsInput input);
    }
}