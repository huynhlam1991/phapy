﻿using System.Collections.Generic;
using Abp.AutoMapper;
using System.Collections.Generic;
using TranLe.PhapY.NhanViens.Dto;

namespace TranLe.PhapY.HoSos.Dto
{
    [AutoMapTo(typeof(Models.BenhNhan))]
    public class HoSoTuThiDto : HoSoBaseDto
    {
        public string NguyenNhanXayRa { get; set; }
        public List<int> NhanVienIds { get; set; }
        public List<HoSoImageDto> HoSoImageDtos { get; set; }
        public string PhuLucTempate { get; set; }
        public List<NhanVienDto> NhanVienDtos { get; set; }
    }
}
