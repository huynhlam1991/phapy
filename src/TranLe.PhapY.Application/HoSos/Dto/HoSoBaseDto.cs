﻿using System;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System.ComponentModel.DataAnnotations;
using TranLe.PhapY.BenhNhans.Dto;

namespace TranLe.PhapY.HoSos.Dto
{
    [AutoMapTo(typeof(Models.HoSo))]
    public class HoSoBaseDto : FullAuditedEntityDto
    {
        [Required(ErrorMessage = "Nhập số hồ sơ")]
        public string SoHoSo { get; set; }

        public int LoaiHoSoId { get; set; }
        [Required(ErrorMessage = "Nhập địa điểm xảy ra")]
        public string DiaDiemXayRa { get; set; }

        [Required(ErrorMessage = "Nhập số quyết định Trưng Cầu Giám Định")]
        [MaxLength(ErrorMessage = "Số quyết định trưng cầu không quá 5")]
        public string SoQuyetDinhTcgd { get; set; }

        [Required(ErrorMessage = "Nhập cơ quan trưng cầu")]
        public string CoQuanTc { get; set; }

        [UIHint("DateTime")]
        [Required(ErrorMessage = "Nhập ngày trưng cầu giám định")]
        public DateTime NgayQuyetDinhTrungCau { get; set; }

        [Required(ErrorMessage = "Nhập địa điểm giám định")]
        public string DiaDiemGiamDinh { get; set; }

        [UIHint("DateTime")]
        [Required(ErrorMessage = "Nhập thời gian giám định")]
        public DateTime NgayGiamDinh { get; set; }
        [Required]
        public string ContentFileName { get; set; }

        public bool IsHaveContent { get; set; }

        public string BacSiGiamDinh { get; set; }
        public BenhNhanDto ThongTinBenhNhan { get; set; }
    }
}
