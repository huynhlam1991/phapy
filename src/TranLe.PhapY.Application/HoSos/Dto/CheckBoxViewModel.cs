﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranLe.PhapY.HoSos.Dto
{
    public class CheckBoxViewModel
    {
        public int NhanVienId { get; set; }
        public string TenNhanVien { get; set; }
        public int ChucVuId { get; set; }
        public bool Checked { get; set; }
        public string TenChucVu { get; set; }
    }
}
