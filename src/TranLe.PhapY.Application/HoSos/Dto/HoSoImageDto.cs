﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace TranLe.PhapY.HoSos.Dto
{
    [AutoMapTo(typeof(Models.HinhAnh))]
    public class HoSoImageDto : EntityDto
    {
        public string TenHinhAnh { get; set; }
        public string ImagePath { get; set; }
        private string _imageUrl;

        public string ImageUrl
        {
            get
            {
                if (string.IsNullOrEmpty(ImagePath))
                    return string.Empty;
                return string.Format(ImagePath, HoSoId) + TenHinhAnh;
            }
        }

        public int ThuTuChon { get; set; }
        public bool DuocChon { get; set; }
        public int HoSoId { get; set; }
    }
}
