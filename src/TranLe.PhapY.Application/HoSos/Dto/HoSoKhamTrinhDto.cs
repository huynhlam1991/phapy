﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;
using TranLe.PhapY.NhanViens.Dto;

namespace TranLe.PhapY.HoSos.Dto
{
    [AutoMapTo(typeof(Models.BenhNhan))]
    public class HoSoKhamTrinhDto : HoSoBaseDto
    {
        [UIHint("Date")]
        public DateTime NgayXayRa { get; set; }
        public List<int> NhanVienIds { get; set; }
        public List<HoSoImageDto> HoSoImageDtos { get; set; }
        public string PhuLucTempate { get; set; }
        public string HoSoDuongSu { get; set; }
        public List<NhanVienDto> NhanVienDtos { get; set; }
    }
}
