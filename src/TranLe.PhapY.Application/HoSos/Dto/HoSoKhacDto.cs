﻿using System;

namespace TranLe.PhapY.HoSos.Dto
{
    public class HoSoKhacDto:HoSoBaseDto
    {
        public int LoaiHoSoId { get; set; }
        public DateTime NgayXayRa { get; set; }
        public string BacSiThamGia { get; set; }
    }
}
