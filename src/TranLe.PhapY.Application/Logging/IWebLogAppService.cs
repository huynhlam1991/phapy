﻿using Abp.Application.Services;
using TranLe.PhapY.Dto;
using TranLe.PhapY.Logging.Dto;

namespace TranLe.PhapY.Logging
{
    public interface IWebLogAppService : IApplicationService
    {
        GetLatestWebLogsOutput GetLatestWebLogs();

        FileDto DownloadWebLogs();
    }
}
