﻿using System.Collections.Generic;

namespace TranLe.PhapY.Logging.Dto
{
    public class GetLatestWebLogsOutput
    {
        public List<string> LatesWebLogLines { get; set; }
    }
}
