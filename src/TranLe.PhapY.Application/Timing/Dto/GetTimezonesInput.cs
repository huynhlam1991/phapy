﻿using Abp.Configuration;

namespace TranLe.PhapY.Timing.Dto
{
    public class GetTimezonesInput
    {
        public SettingScopes DefaultTimezoneScope;
    }
}
