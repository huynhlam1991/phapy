﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System.ComponentModel.DataAnnotations;

namespace TranLe.PhapY.ChucVus.Dto
{
    [AutoMapTo(typeof(Models.ChucVu))]
    public class ChucVuDto : EntityDto
    {
        [Required(ErrorMessage = "Nhập tên chức vụ")]
        [StringLength(100, ErrorMessage = "Số ký tự tối đa là 100")]
        public string TenChucVu { get; set; }
        [StringLength(200, ErrorMessage = "Số ký tự tối đa là 200")]
        public string DienGiai { get; set; }
    }
}
