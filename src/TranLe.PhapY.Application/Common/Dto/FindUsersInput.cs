﻿using TranLe.PhapY.Dto;

namespace TranLe.PhapY.Common.Dto
{
    public class FindUsersInput : PagedAndFilteredInputDto
    {
        public int? TenantId { get; set; }
    }
}