﻿using TranLe.PhapY.Security;

namespace TranLe.PhapY.Authorization.Users.Profile.Dto
{
    public class GetPasswordComplexitySettingOutput
    {
        public PasswordComplexitySetting Setting { get; set; }
    }
}
