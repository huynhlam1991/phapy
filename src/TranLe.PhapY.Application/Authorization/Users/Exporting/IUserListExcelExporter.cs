using System.Collections.Generic;
using TranLe.PhapY.Authorization.Users.Dto;
using TranLe.PhapY.Dto;

namespace TranLe.PhapY.Authorization.Users.Exporting
{
    public interface IUserListExcelExporter
    {
        FileDto ExportToFile(List<UserListDto> userListDtos);
    }
}