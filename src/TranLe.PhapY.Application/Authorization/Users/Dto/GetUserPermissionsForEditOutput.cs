﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using TranLe.PhapY.Authorization.Permissions.Dto;

namespace TranLe.PhapY.Authorization.Users.Dto
{
    public class GetUserPermissionsForEditOutput
    {
        public List<FlatPermissionDto> Permissions { get; set; }

        public List<string> GrantedPermissionNames { get; set; }
    }
}