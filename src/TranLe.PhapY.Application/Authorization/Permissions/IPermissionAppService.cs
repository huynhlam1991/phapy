﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TranLe.PhapY.Authorization.Permissions.Dto;

namespace TranLe.PhapY.Authorization.Permissions
{
    public interface IPermissionAppService : IApplicationService
    {
        ListResultDto<FlatPermissionWithLevelDto> GetAllPermissions();
    }
}
