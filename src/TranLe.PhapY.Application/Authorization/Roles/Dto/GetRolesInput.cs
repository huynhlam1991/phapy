﻿using Abp.Application.Services.Dto;

namespace TranLe.PhapY.Authorization.Roles.Dto
{
    public class GetRolesInput 
    {
        public string Permission { get; set; }
    }
}
