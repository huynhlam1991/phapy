﻿using System;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System.ComponentModel.DataAnnotations;

namespace TranLe.PhapY.BenhNhans.Dto
{
    [AutoMapTo(typeof(Models.BenhNhan))]
    public class BenhNhanDto : EntityDto
    {
        [Required(ErrorMessage = "Nhập họ tên")]
        public string HoTen { get; set; }

        [Required(ErrorMessage = "Chọn giới tính")]
        public bool GioiTinh { get; set; }

        [UIHint("Date")]
        [Required(ErrorMessage = "Nhập ngày sinh")]
        public DateTime NamSinh { get; set; }

        [Required(ErrorMessage = "Nhập địa chỉ")]
        public string DiaChi { get; set; }

        [Required(ErrorMessage = "Nhập số điện thoại")]
        public string SoDt { get; set; }

        public ThongTinCaNhan ThongTinCaNhan { get; set; }
    }
    public class ThongTinCaNhan
    {
        public string AvatarName { get; set; }
        public string QuocTich { get; set; }
        public string DanToc { get; set; }
        public string TrinhDoVanHoa { get; set; }
        public string NgheNghiep { get; set; }
        public string TonGiao { get; set; }
    }
}
