﻿using Abp.Notifications;
using TranLe.PhapY.Dto;

namespace TranLe.PhapY.Notifications.Dto
{
    public class GetUserNotificationsInput : PagedInputDto
    {
        public UserNotificationState? State { get; set; }
    }
}