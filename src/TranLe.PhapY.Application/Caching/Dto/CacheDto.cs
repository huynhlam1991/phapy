﻿using Abp.Application.Services.Dto;

namespace TranLe.PhapY.Caching.Dto
{
    public class CacheDto
    {
        public string Name { get; set; }
    }
}
