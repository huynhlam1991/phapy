﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System.ComponentModel.DataAnnotations;

namespace TranLe.PhapY.LoaiHoSos.Dto
{
    [AutoMapTo(typeof(Models.LoaiHoSo))]
    public class LoaiHoSoDto : EntityDto
    {
        [Required(ErrorMessage = "Nhập tên loại hồ sơ")]
        [StringLength(100, ErrorMessage = "Số ký tự tối đa là 100")]
        //[RegularExpression(@"^[0-9a-zA-Z''-'\s]{1,40}$", ErrorMessage = "Không được chứa ký tự đặc biệt")]
        public string TenLoaiHs { get; set; }
    }
}
