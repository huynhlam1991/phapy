using System.Data.Entity;
using System.Reflection;
using Abp.Events.Bus;
using Abp.Modules;
using Castle.MicroKernel.Registration;
using TranLe.PhapY.EntityFramework;

namespace TranLe.PhapY.Migrator
{
    [DependsOn(typeof(PhapYDataModule))]
    public class PhapYMigratorModule : AbpModule
    {
        public override void PreInitialize()
        {
            Database.SetInitializer<PhapYDbContext>(null);

            Configuration.BackgroundJobs.IsJobExecutionEnabled = false;
            Configuration.ReplaceService(typeof(IEventBus), () =>
            {
                IocManager.IocContainer.Register(
                    Component.For<IEventBus>().Instance(NullEventBus.Instance)
                );
            });
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}