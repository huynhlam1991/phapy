﻿using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TranLe.PhapY.Models
{
    [Table("HinhAnh")]
    public class HinhAnh : FullAuditedEntity
    {
        [Required]
        [MaxLength(100)]
        public string TenHinhAnh { get; set; }
        public int ThuTuChon { get; set; }
        public bool DuocChon { get; set; }
        public int HoSoId { get; set; }
        [ForeignKey("HoSoId")]
        public virtual HoSo HoSo { get; set; }
    }
}
