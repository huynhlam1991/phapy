﻿using System.Threading.Tasks;
using Abp.Configuration;

namespace TranLe.PhapY.Timing
{
    public interface ITimeZoneService
    {
        Task<string> GetDefaultTimezoneAsync(SettingScopes scope, int? tenantId);
    }
}
