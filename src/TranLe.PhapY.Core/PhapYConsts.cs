﻿namespace TranLe.PhapY
{
    /// <summary>
    /// Some general constants for the application.
    /// </summary>
    public class PhapYConsts
    {
        public const string LocalizationSourceName = "PhapY";

        public const bool MultiTenancyEnabled = false;
    }
}