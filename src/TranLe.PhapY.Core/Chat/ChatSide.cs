﻿namespace TranLe.PhapY.Chat
{
    public enum ChatSide
    {
        Sender = 1,

        Receiver = 2
    }
}