﻿namespace TranLe.PhapY.Chat
{
    public enum ChatMessageReadState
    {
        Unread = 1,

        Read = 2
    }
}