﻿using Abp.Domain.Services;

namespace TranLe.PhapY
{
    public abstract class PhapYDomainServiceBase : DomainService
    {
        /* Add your common members for all your domain services. */

        protected PhapYDomainServiceBase()
        {
            LocalizationSourceName = PhapYConsts.LocalizationSourceName;
        }
    }
}
