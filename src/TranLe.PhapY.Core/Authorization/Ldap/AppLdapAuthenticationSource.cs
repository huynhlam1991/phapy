﻿using Abp.Zero.Ldap.Authentication;
using Abp.Zero.Ldap.Configuration;
using TranLe.PhapY.Authorization.Users;
using TranLe.PhapY.MultiTenancy;

namespace TranLe.PhapY.Authorization.Ldap
{
    public class AppLdapAuthenticationSource : LdapAuthenticationSource<Tenant, User>
    {
        public AppLdapAuthenticationSource(ILdapSettings settings, IAbpZeroLdapModuleConfig ldapModuleConfig)
            : base(settings, ldapModuleConfig)
        {
        }
    }
}
