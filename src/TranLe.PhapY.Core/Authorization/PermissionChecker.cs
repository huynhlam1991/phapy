﻿using Abp.Authorization;
using TranLe.PhapY.Authorization.Roles;
using TranLe.PhapY.Authorization.Users;
using TranLe.PhapY.MultiTenancy;

namespace TranLe.PhapY.Authorization
{
    /// <summary>
    /// Implements <see cref="PermissionChecker"/>.
    /// </summary>
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {

        }
    }
}
