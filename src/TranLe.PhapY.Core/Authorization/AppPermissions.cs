﻿namespace TranLe.PhapY.Authorization
{
    /// <summary>
    /// Defines string constants for application's permission names.
    /// <see cref="AppAuthorizationProvider"/> for permission definitions.
    /// </summary>
    public static class AppPermissions
    {
        //COMMON PERMISSIONS (FOR BOTH OF TENANTS AND HOST)

        public const string Pages = "Pages";
        
        public const string Pages_Administration = "Pages.Administration";

        public const string Pages_Administration_Roles = "Pages.Administration.Roles";
        public const string Pages_Administration_Roles_Create = "Pages.Administration.Roles.Create";
        public const string Pages_Administration_Roles_Edit = "Pages.Administration.Roles.Edit";
        public const string Pages_Administration_Roles_Delete = "Pages.Administration.Roles.Delete";

        public const string Pages_Administration_Users = "Pages.Administration.Users";
        public const string Pages_Administration_Users_Create = "Pages.Administration.Users.Create";
        public const string Pages_Administration_Users_Edit = "Pages.Administration.Users.Edit";
        public const string Pages_Administration_Users_Delete = "Pages.Administration.Users.Delete";
        public const string Pages_Administration_Users_ChangePermissions = "Pages.Administration.Users.ChangePermissions";
        public const string Pages_Administration_Users_Impersonation = "Pages.Administration.Users.Impersonation";

        public const string Pages_Administration_Languages = "Pages.Administration.Languages";
        public const string Pages_Administration_Languages_Create = "Pages.Administration.Languages.Create";
        public const string Pages_Administration_Languages_Edit = "Pages.Administration.Languages.Edit";
        public const string Pages_Administration_Languages_Delete = "Pages.Administration.Languages.Delete";
        public const string Pages_Administration_Languages_ChangeTexts = "Pages.Administration.Languages.ChangeTexts";

        public const string Pages_Administration_AuditLogs = "Pages.Administration.AuditLogs";

        public const string Pages_Administration_OrganizationUnits = "Pages.Administration.OrganizationUnits";
        public const string Pages_Administration_OrganizationUnits_ManageOrganizationTree = "Pages.Administration.OrganizationUnits.ManageOrganizationTree";
        public const string Pages_Administration_OrganizationUnits_ManageMembers = "Pages.Administration.OrganizationUnits.ManageMembers";

        public const string Pages_Administration_HangfireDashboard = "Pages.Administration.HangfireDashboard";

        //TENANT-SPECIFIC PERMISSIONS

        public const string Pages_Tenant_Dashboard = "Pages.Tenant.Dashboard";

        public const string Pages_Administration_Tenant_Settings = "Pages.Administration.Tenant.Settings";

        public const string Pages_NhanVien_Read = "Pages.NhanVien.Read";
        public const string Pages_NhanVien_Create = "Pages.NhanVien.Create";
        public const string Pages_NhanVien_Update = "Pages.NhanVien.Update";
        public const string Pages_NhanVien_Delete = "Pages.NhanVien.Delete";
        public const string Pages_HoSoTuThi_Read = "Pages.HoSoTuThi.Read";
        public const string Pages_HoSoTuThi_Create = "Pages.HoSoTuThi.Create";
        public const string Pages_HoSoTuThi_Update = "Pages.HoSoTuThi.Update";
        public const string Pages_HoSoTuThi_Delete = "Pages.HoSoTuThi.Delate";
        public const string Pages_HoSoKhamTrinh_Read = "Pages.HoSoKhamTrinh.Read";
        public const string Pages_HoSoKhamTrinh_Create = "Pages.HoSoKhamTrinh.Create";
        public const string Pages_HoSoKhamTrinh_Update = "Pages.HoSoKhamTrinh.Update";
        public const string Pages_HoSoKhamTrinh_Delete = "Pages.HoSoKhamTrinh.Delate";
        public const string Pages_HoSoThuongTat_Read = "Pages.HoSoThuongTat.Read";
        public const string Pages_HoSoThuongTat_Create = "Pages.HoSoThuongTat.Create";
        public const string Pages_HoSoThuongTat_Update = "Pages.HoSoThuongTat.Update";
        public const string Pages_HoSoThuongTat_Delete = "Pages.HoSoThuongTat.Delate";
        public const string Pages_TrinhDo_Read = "Pages.TrinhDo.Read";
        public const string Pages_TrinhDo_Create = "Pages.TrinhDo.Create";
        public const string Pages_TrinhDo_Update = "Pages.TrinhDo.Update";
        public const string Pages_TrinhDo_Delete = "Pages.TrinhDo.Delete";
        public const string Pages_ChucVu_Read = "Pages.ChucVu.Read";
        public const string Pages_ChucVu_Create = "Pages.ChucVu.Create";
        public const string Pages_ChucVu_Update = "Pages.ChucVu.Update";
        public const string Pages_ChucVu_Delete = "Pages.ChucVu.Delete";
        public const string Pages_LoaiHoSo_Read = "Pages.LoaiHoSo.Read";
        public const string Pages_LoaiHoSo_Create = "Pages.LoaiHoSo.Create";
        public const string Pages_LoaiHoSo_Update = "Pages.LoaiHoSo.Update";
        public const string Pages_LoaiHoSo_Delete = "Pages.LoaiHoSo.Delete";
        public const string Pages_PhuLucHoSo_Read = "Pages.PhuLucHoSo.Read";
        public const string Pages_PhuLucHoSo_Create = "Pages.PhuLucHoSo.Create";
        public const string Pages_PhuLucHoSo_Update = "Pages.PhuLucHoSo.Update";
        public const string Pages_PhuLucHoSo_Delete = "Pages.PhuLucHoSo.Delete";
        public const string Pages_BenhNhan_Read = "Pages.BenhNhan.Read";
        public const string Pages_BenhNhan_Create = "Pages.BenhNhan.Create";
        public const string Pages_BenhNhan_Update = "Pages.BenhNhan.Update";
        public const string Pages_BenhNhan_Delete = "Pages.BenhNhan.Delete";

        //HOST-SPECIFIC PERMISSIONS

        public const string Pages_Editions = "Pages.Editions";
        public const string Pages_Editions_Create = "Pages.Editions.Create";
        public const string Pages_Editions_Edit = "Pages.Editions.Edit";
        public const string Pages_Editions_Delete = "Pages.Editions.Delete";

        public const string Pages_Tenants = "Pages.Tenants";
        public const string Pages_Tenants_Create = "Pages.Tenants.Create";
        public const string Pages_Tenants_Edit = "Pages.Tenants.Edit";
        public const string Pages_Tenants_ChangeFeatures = "Pages.Tenants.ChangeFeatures";
        public const string Pages_Tenants_Delete = "Pages.Tenants.Delete";
        public const string Pages_Tenants_Impersonation = "Pages.Tenants.Impersonation";

        public const string Pages_Administration_Host_Maintenance = "Pages.Administration.Host.Maintenance";
        public const string Pages_Administration_Host_Settings = "Pages.Administration.Host.Settings";
    }
}