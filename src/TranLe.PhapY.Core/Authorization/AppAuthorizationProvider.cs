﻿using Abp.Authorization;
using Abp.Configuration.Startup;
using Abp.Localization;
using Abp.MultiTenancy;

namespace TranLe.PhapY.Authorization
{
    /// <summary>
    /// Application's authorization provider.
    /// Defines permissions for the application.
    /// See <see cref="AppPermissions"/> for all permission names.
    /// </summary>
    public class AppAuthorizationProvider : AuthorizationProvider
    {
        private readonly bool _isMultiTenancyEnabled;

        public AppAuthorizationProvider(bool isMultiTenancyEnabled)
        {
            _isMultiTenancyEnabled = isMultiTenancyEnabled;
        }

        public AppAuthorizationProvider(IMultiTenancyConfig multiTenancyConfig)
        {
            _isMultiTenancyEnabled = multiTenancyConfig.IsEnabled;
        }

        public override void SetPermissions(IPermissionDefinitionContext context)
        {
            //COMMON PERMISSIONS (FOR BOTH OF TENANTS AND HOST)

            var pages = context.GetPermissionOrNull(AppPermissions.Pages) ?? context.CreatePermission(AppPermissions.Pages, L("Pages"));

            var administration = pages.CreateChildPermission(AppPermissions.Pages_Administration, L("Administration"));

            var roles = administration.CreateChildPermission(AppPermissions.Pages_Administration_Roles, L("Roles"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Create, L("CreatingNewRole"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Edit, L("EditingRole"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Delete, L("DeletingRole"));

            var users = administration.CreateChildPermission(AppPermissions.Pages_Administration_Users, L("Users"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Create, L("CreatingNewUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Edit, L("EditingUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Delete, L("DeletingUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_ChangePermissions, L("ChangingPermissions"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Impersonation, L("LoginForUsers"));

            var languages = administration.CreateChildPermission(AppPermissions.Pages_Administration_Languages, L("Languages"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Create, L("CreatingNewLanguage"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Edit, L("EditingLanguage"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Delete, L("DeletingLanguages"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_ChangeTexts, L("ChangingTexts"));

            administration.CreateChildPermission(AppPermissions.Pages_Administration_AuditLogs, L("AuditLogs"));

            var organizationUnits = administration.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits, L("OrganizationUnits"));
            organizationUnits.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits_ManageOrganizationTree, L("ManagingOrganizationTree"));
            organizationUnits.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits_ManageMembers, L("ManagingMembers"));

            //TENANT-SPECIFIC PERMISSIONS

            pages.CreateChildPermission(AppPermissions.Pages_Tenant_Dashboard, L("Dashboard"), multiTenancySides: MultiTenancySides.Tenant);

            administration.CreateChildPermission(AppPermissions.Pages_Administration_Tenant_Settings, L("Settings"), multiTenancySides: MultiTenancySides.Tenant);

            var pageNhanVien = context.CreatePermission(AppPermissions.Pages_NhanVien_Read, L("XemDanhSachNhanVien"));
            pageNhanVien.CreateChildPermission(AppPermissions.Pages_NhanVien_Create, L("TaoMoiNhanVien"));
            pageNhanVien.CreateChildPermission(AppPermissions.Pages_NhanVien_Update, L("CapNhatNhanVien"));
            pageNhanVien.CreateChildPermission(AppPermissions.Pages_NhanVien_Delete, L("XoaNhanVien"));
            var pageHoSoTuThi = context.CreatePermission(AppPermissions.Pages_HoSoTuThi_Read, L("XemDanhSachHoSoTuThi"));
            pageHoSoTuThi.CreateChildPermission(AppPermissions.Pages_HoSoTuThi_Create, L("TaoMoiHoSoTuThi"));
            pageHoSoTuThi.CreateChildPermission(AppPermissions.Pages_HoSoTuThi_Update, L("CapNhatHoSoTuThi"));
            pageHoSoTuThi.CreateChildPermission(AppPermissions.Pages_HoSoTuThi_Delete, L("XoaHoSoTuThi"));
            var pageHoSoKhamTrinh = context.CreatePermission(AppPermissions.Pages_HoSoKhamTrinh_Read, L("XemDanhSachHoSoKhamTrinh"));
            pageHoSoKhamTrinh.CreateChildPermission(AppPermissions.Pages_HoSoKhamTrinh_Create, L("TaoMoiHoSoKhamTrinh"));
            pageHoSoKhamTrinh.CreateChildPermission(AppPermissions.Pages_HoSoKhamTrinh_Update, L("CapNhatHoSoKhamTrinh"));
            pageHoSoKhamTrinh.CreateChildPermission(AppPermissions.Pages_HoSoKhamTrinh_Delete, L("XoaHoSoKhamTrinh"));
            var pageHoSoThuongTat = context.CreatePermission(AppPermissions.Pages_HoSoThuongTat_Read, L("XemDanhSachHoSoThuongTat"));
            pageHoSoThuongTat.CreateChildPermission(AppPermissions.Pages_HoSoThuongTat_Create, L("TaoMoiHoSoThuongTat"));
            pageHoSoThuongTat.CreateChildPermission(AppPermissions.Pages_HoSoThuongTat_Update, L("CapNhatHoSoThuongTat"));
            pageHoSoThuongTat.CreateChildPermission(AppPermissions.Pages_HoSoThuongTat_Delete, L("XoaHoSoThuongTat"));
            var pageTrinhDo = context.CreatePermission(AppPermissions.Pages_TrinhDo_Read, L("XemDanhSachTrinhDo"));
            pageTrinhDo.CreateChildPermission(AppPermissions.Pages_TrinhDo_Create, L("TaoMoiTrinhDo"));
            pageTrinhDo.CreateChildPermission(AppPermissions.Pages_TrinhDo_Update, L("CapNhatTrinhDo"));
            pageTrinhDo.CreateChildPermission(AppPermissions.Pages_TrinhDo_Delete, L("XoaTrinhDo"));
            var pageChucVu = context.CreatePermission(AppPermissions.Pages_ChucVu_Read, L("XemDanhSachChucVu"));
            pageChucVu.CreateChildPermission(AppPermissions.Pages_ChucVu_Create, L("TaoMoiChucVu"));
            pageChucVu.CreateChildPermission(AppPermissions.Pages_ChucVu_Update, L("CapNhatChucVu"));
            pageChucVu.CreateChildPermission(AppPermissions.Pages_ChucVu_Delete, L("XoaChucVu"));
            var pageLoaiHoSo = context.CreatePermission(AppPermissions.Pages_LoaiHoSo_Read, L("XemDanhSachLoaiHoSo"));
            pageLoaiHoSo.CreateChildPermission(AppPermissions.Pages_LoaiHoSo_Create, L("TaoMoiLoaiHoSo"));
            pageLoaiHoSo.CreateChildPermission(AppPermissions.Pages_LoaiHoSo_Update, L("CapNhatLoaiHoSo"));
            pageLoaiHoSo.CreateChildPermission(AppPermissions.Pages_LoaiHoSo_Delete, L("XoaLoaiHoSo"));
            var pagePhuLucHoSo = context.CreatePermission(AppPermissions.Pages_PhuLucHoSo_Read, L("XemDanhSachPhuLucHoSo"));
            pagePhuLucHoSo.CreateChildPermission(AppPermissions.Pages_PhuLucHoSo_Create, L("TaoMoiPhuLucHoSo"));
            pagePhuLucHoSo.CreateChildPermission(AppPermissions.Pages_PhuLucHoSo_Update, L("CapNhatPhuLucHoSo"));
            pagePhuLucHoSo.CreateChildPermission(AppPermissions.Pages_PhuLucHoSo_Delete, L("XoaPhuLucHoSo"));
            var pageBenhNhan = context.CreatePermission(AppPermissions.Pages_BenhNhan_Read, L("XemDanhSachBenhNhan"));
            pageBenhNhan.CreateChildPermission(AppPermissions.Pages_BenhNhan_Create, L("TaoMoiBenhNhan"));
            pageBenhNhan.CreateChildPermission(AppPermissions.Pages_BenhNhan_Update, L("CapNhatBenhNhan"));
            pageBenhNhan.CreateChildPermission(AppPermissions.Pages_BenhNhan_Delete, L("XoaBenhNhan"));

            //HOST-SPECIFIC PERMISSIONS

            var editions = pages.CreateChildPermission(AppPermissions.Pages_Editions, L("Editions"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_Create, L("CreatingNewEdition"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_Edit, L("EditingEdition"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_Delete, L("DeletingEdition"), multiTenancySides: MultiTenancySides.Host);

            var tenants = pages.CreateChildPermission(AppPermissions.Pages_Tenants, L("Tenants"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Create, L("CreatingNewTenant"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Edit, L("EditingTenant"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_ChangeFeatures, L("ChangingFeatures"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Delete, L("DeletingTenant"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Impersonation, L("LoginForTenants"), multiTenancySides: MultiTenancySides.Host);

            administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_Settings, L("Settings"), multiTenancySides: MultiTenancySides.Host);

            administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_Maintenance, L("Maintenance"), multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);

            administration.CreateChildPermission(AppPermissions.Pages_Administration_HangfireDashboard, L("HangfireDashboard"), multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, PhapYConsts.LocalizationSourceName);
        }
    }
}
