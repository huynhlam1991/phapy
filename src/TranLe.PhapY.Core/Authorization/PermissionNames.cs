﻿namespace PhapY.Authorization
{
    public static class PermissionNames
    {
        public const string Pages_Tenants = "Pages.Tenants";

        public const string Pages_Users = "Pages.Users";

        public const string Pages_Roles = "Pages.Roles";

        public const string Pages_NhanVien_Read = "Pages.NhanVien.Read";
        public const string Pages_NhanVien_Create = "Pages.NhanVien.Create";
        public const string Pages_NhanVien_Update = "Pages.NhanVien.Update";
        public const string Pages_NhanVien_Delete = "Pages.NhanVien.Delete";
        public const string Pages_HoSo_Read = "Pages.HoSo.Read";
        public const string Pages_HoSo_Create = "Pages.HoSo.Create";
        public const string Pages_HoSo_Update = "Pages.HoSo.Update";
        public const string Pages_HoSo_Delete = "Pages.HoSo.Delate";
        public const string Pages_TrinhDo_Read = "Pages.TrinhDo.Read";
        public const string Pages_TrinhDo_Create = "Pages.TrinhDo.Create";
        public const string Pages_TrinhDo_Update = "Pages.TrinhDo.Update";
        public const string Pages_TrinhDo_Delete = "Pages.TrinhDo.Delete";
        public const string Pages_ChucVu_Read = "Pages.ChucVu.Read";
        public const string Pages_ChucVu_Create = "Pages.ChucVu.Create";
        public const string Pages_ChucVu_Update = "Pages.ChucVu.Update";
        public const string Pages_ChucVu_Delete = "Pages.ChucVu.Delete";
        public const string Pages_LoaiHoSo_Read = "Pages.LoaiHoSo.Read";
        public const string Pages_LoaiHoSo_Create = "Pages.LoaiHoSo.Create";
        public const string Pages_LoaiHoSo_Update = "Pages.LoaiHoSo.Update";
        public const string Pages_LoaiHoSo_Delete = "Pages.LoaiHoSo.Delete";
        public const string Pages_PhuLucHoSo_Read = "Pages.PhuLucHoSo.Read";
        public const string Pages_PhuLucHoSo_Create = "Pages.PhuLucHoSo.Create";
        public const string Pages_PhuLucHoSo_Update = "Pages.PhuLucHoSo.Update";
        public const string Pages_PhuLucHoSo_Delete = "Pages.PhuLucHoSo.Delete";
        public const string Pages_BenhNhan_Read = "Pages.BenhNhan.Read";
        public const string Pages_BenhNhan_Create = "Pages.BenhNhan.Create";
        public const string Pages_BenhNhan_Update = "Pages.BenhNhan.Update";
        public const string Pages_BenhNhan_Delete = "Pages.BenhNhan.Delete";
    }
}