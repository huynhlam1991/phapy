﻿using System.Collections.Generic;
using Abp.Dependency;
using Abp.RealTime;

namespace TranLe.PhapY.Authorization.Users
{
    public interface IUserLogoutInformer
    {
        void InformClients(IReadOnlyList<IOnlineClient> clients);
    }
}
