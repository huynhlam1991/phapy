﻿using System.Threading.Tasks;
using Abp.Domain.Policies;

namespace TranLe.PhapY.Authorization.Users
{
    public interface IUserPolicy : IPolicy
    {
        Task CheckMaxUserCountAsync(int tenantId);
    }
}
