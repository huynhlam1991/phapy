﻿using Abp.AutoMapper;
using Abp.Organizations;

namespace TranLe.PhapY.Web.Areas.Admin.Models.OrganizationUnits
{
    [AutoMapFrom(typeof(OrganizationUnit))]
    public class EditOrganizationUnitModalViewModel
    {
        public long? Id { get; set; }

        public string DisplayName { get; set; }
    }
}