﻿using Abp.AutoMapper;
using TranLe.PhapY.Authorization.Roles.Dto;
using TranLe.PhapY.Web.Areas.Admin.Models.Common;

namespace TranLe.PhapY.Web.Areas.Admin.Models.Roles
{
    [AutoMapFrom(typeof(GetRoleForEditOutput))]
    public class CreateOrEditRoleModalViewModel : GetRoleForEditOutput, IPermissionsEditViewModel
    {
        public bool IsEditMode
        {
            get { return Role.Id.HasValue; }
        }

        public CreateOrEditRoleModalViewModel(GetRoleForEditOutput output)
        {
            output.MapTo(this);
        }
    }
}