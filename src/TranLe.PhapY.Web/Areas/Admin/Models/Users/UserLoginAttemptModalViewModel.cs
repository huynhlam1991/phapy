using System.Collections.Generic;
using TranLe.PhapY.Authorization.Users.Dto;

namespace TranLe.PhapY.Web.Areas.Admin.Models.Users
{
    public class UserLoginAttemptModalViewModel
    {
        public List<UserLoginAttemptDto> LoginAttempts { get; set; }
    }
}