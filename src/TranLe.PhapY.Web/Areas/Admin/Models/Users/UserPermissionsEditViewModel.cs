using Abp.AutoMapper;
using TranLe.PhapY.Authorization.Users;
using TranLe.PhapY.Authorization.Users.Dto;
using TranLe.PhapY.Web.Areas.Admin.Models.Common;

namespace TranLe.PhapY.Web.Areas.Admin.Models.Users
{
    [AutoMapFrom(typeof(GetUserPermissionsForEditOutput))]
    public class UserPermissionsEditViewModel : GetUserPermissionsForEditOutput, IPermissionsEditViewModel
    {
        public User User { get; private set; }

        public UserPermissionsEditViewModel(GetUserPermissionsForEditOutput output, User user)
        {
            User = user;
            output.MapTo(this);
        }
    }
}