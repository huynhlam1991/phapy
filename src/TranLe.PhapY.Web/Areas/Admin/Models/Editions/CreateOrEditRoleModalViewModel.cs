﻿using Abp.AutoMapper;
using TranLe.PhapY.Editions.Dto;
using TranLe.PhapY.Web.Areas.Admin.Models.Common;

namespace TranLe.PhapY.Web.Areas.Admin.Models.Editions
{
    [AutoMapFrom(typeof(GetEditionForEditOutput))]
    public class CreateOrEditEditionModalViewModel : GetEditionForEditOutput, IFeatureEditViewModel
    {
        public bool IsEditMode
        {
            get { return Edition.Id.HasValue; }
        }

        public CreateOrEditEditionModalViewModel(GetEditionForEditOutput output)
        {
            output.MapTo(this);
        }
    }
}