using Abp.AutoMapper;
using TranLe.PhapY.MultiTenancy;
using TranLe.PhapY.MultiTenancy.Dto;
using TranLe.PhapY.Web.Areas.Admin.Models.Common;

namespace TranLe.PhapY.Web.Areas.Admin.Models.Tenants
{
    [AutoMapFrom(typeof (GetTenantFeaturesForEditOutput))]
    public class TenantFeaturesEditViewModel : GetTenantFeaturesForEditOutput, IFeatureEditViewModel
    {
        public Tenant Tenant { get; set; }

        public TenantFeaturesEditViewModel(Tenant tenant, GetTenantFeaturesForEditOutput output)
        {
            Tenant = tenant;
            output.MapTo(this);
        }
    }
}