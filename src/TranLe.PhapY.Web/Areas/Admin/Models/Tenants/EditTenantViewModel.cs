﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using TranLe.PhapY.MultiTenancy.Dto;

namespace TranLe.PhapY.Web.Areas.Admin.Models.Tenants
{
    public class EditTenantViewModel
    {
        public TenantEditDto Tenant { get; set; }

        public IReadOnlyList<ComboboxItemDto> EditionItems { get; set; }

        public EditTenantViewModel(TenantEditDto tenant, IReadOnlyList<ComboboxItemDto> editionItems)
        {
            Tenant = tenant;
            EditionItems = editionItems;
        }
    }
}