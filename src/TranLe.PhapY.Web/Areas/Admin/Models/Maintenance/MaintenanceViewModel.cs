﻿using System.Collections.Generic;
using TranLe.PhapY.Caching.Dto;

namespace TranLe.PhapY.Web.Areas.Admin.Models.Maintenance
{
    public class MaintenanceViewModel
    {
        public IReadOnlyList<CacheDto> Caches { get; set; }
    }
}