﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using TranLe.PhapY.Configuration.Host.Dto;

namespace TranLe.PhapY.Web.Areas.Admin.Models.HostSettings
{
    public class HostSettingsViewModel
    {
        public HostSettingsEditDto Settings { get; set; }

        public List<ComboboxItemDto> EditionItems { get; set; }

        public List<ComboboxItemDto> TimezoneItems { get; set; }
    }
}