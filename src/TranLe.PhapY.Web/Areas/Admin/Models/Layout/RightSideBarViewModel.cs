﻿using TranLe.PhapY.Configuration.Ui;

namespace TranLe.PhapY.Web.Areas.Admin.Models.Layout
{
    public class RightSideBarViewModel
    {
        public UiThemeInfo CurrentTheme { get; set; }
    }
}