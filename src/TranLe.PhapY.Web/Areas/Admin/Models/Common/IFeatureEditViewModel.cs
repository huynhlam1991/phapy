using System.Collections.Generic;
using Abp.Application.Services.Dto;
using TranLe.PhapY.Editions.Dto;

namespace TranLe.PhapY.Web.Areas.Admin.Models.Common
{
    public interface IFeatureEditViewModel
    {
        List<NameValueDto> FeatureValues { get; set; }

        List<FlatFeatureDto> Features { get; set; }
    }
}