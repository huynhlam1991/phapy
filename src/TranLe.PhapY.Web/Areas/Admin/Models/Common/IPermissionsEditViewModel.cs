using System.Collections.Generic;
using TranLe.PhapY.Authorization.Permissions.Dto;

namespace TranLe.PhapY.Web.Areas.Admin.Models.Common
{
    public interface IPermissionsEditViewModel
    {
        List<FlatPermissionDto> Permissions { get; set; }

        List<string> GrantedPermissionNames { get; set; }
    }
}