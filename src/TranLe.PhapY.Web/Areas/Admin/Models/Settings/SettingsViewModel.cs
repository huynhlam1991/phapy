﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using TranLe.PhapY.Configuration.Tenants.Dto;

namespace TranLe.PhapY.Web.Areas.Admin.Models.Settings
{
    public class SettingsViewModel
    {
        public TenantSettingsEditDto Settings { get; set; }
        
        public List<ComboboxItemDto> TimezoneItems { get; set; }
    }
}