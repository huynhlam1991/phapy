﻿using Abp.Application.Navigation;
using Abp.Localization;
using TranLe.PhapY.Authorization;
using TranLe.PhapY.Web.Navigation;

namespace TranLe.PhapY.Web.Areas.Admin.Startup
{
    public class MpaNavigationProvider : NavigationProvider
    {
        public const string MenuName = "Mpa";
        
        public override void SetNavigation(INavigationProviderContext context)
        {
            var menu = context.Manager.Menus[MenuName] = new MenuDefinition(MenuName, new FixedLocalizableString("Main Menu"));

            menu
                .AddItem(new MenuItemDefinition(
                    PageNames.App.Host.Tenants,
                    L("Tenants"),
                    url: "Admin/Tenants",
                    icon: "icon-globe",
                    requiredPermissionName: AppPermissions.Pages_Tenants
                    )
                ).AddItem(new MenuItemDefinition(
                    PageNames.App.Host.Editions,
                    L("Editions"),
                    url: "Admin/Editions",
                    icon: "icon-grid",
                    requiredPermissionName: AppPermissions.Pages_Editions
                    )
                ).AddItem(new MenuItemDefinition(
                    PageNames.App.Tenant.Dashboard,
                    L("Dashboard"),
                    url: "Admin/Dashboard",
                    icon: "icon-home",
                    requiredPermissionName: AppPermissions.Pages_Tenant_Dashboard
                    )
                ).AddItem(new MenuItemDefinition(
                    PageNames.App.Common.Administration,
                    L("Administration"),
                    icon: "icon-wrench"
                    )
                    //.AddItem(new MenuItemDefinition(
                    //    PageNames.App.Common.OrganizationUnits,
                    //    L("OrganizationUnits"),
                    //    url: "Admin/OrganizationUnits",
                    //    icon: "icon-layers",
                    //    requiredPermissionName: AppPermissions.Pages_Administration_OrganizationUnits
                    //    )
                    //)
                    .AddItem(new MenuItemDefinition(
                        PageNames.App.Common.Roles,
                        L("Roles"),
                        url: "Admin/Roles",
                        icon: "icon-briefcase",
                        requiredPermissionName: AppPermissions.Pages_Administration_Roles
                        )
                    ).AddItem(new MenuItemDefinition(
                        PageNames.App.Common.Users,
                        L("Users"),
                        url: "Admin/Users",
                        icon: "icon-users",
                        requiredPermissionName: AppPermissions.Pages_Administration_Users
                        )
                    )
                    //.AddItem(new MenuItemDefinition(
                    //    PageNames.App.Common.Languages,
                    //    L("Languages"),
                    //    url: "Admin/Languages",
                    //    icon: "icon-flag",
                    //    requiredPermissionName: AppPermissions.Pages_Administration_Languages
                    //    )
                    //)
                    //.AddItem(new MenuItemDefinition(
                    //    PageNames.App.Common.AuditLogs,
                    //    L("AuditLogs"),
                    //    url: "Admin/AuditLogs",
                    //    icon: "icon-lock",
                    //    requiredPermissionName: AppPermissions.Pages_Administration_AuditLogs
                    //    )
                    //).AddItem(new MenuItemDefinition(
                    //    PageNames.App.Host.Maintenance,
                    //    L("Maintenance"),
                    //    url: "Admin/Maintenance",
                    //    icon: "icon-wrench",
                    //    requiredPermissionName: AppPermissions.Pages_Administration_Host_Maintenance
                    //    )
                    //)
                    //.AddItem(new MenuItemDefinition(
                    //    PageNames.App.Host.Settings,
                    //    L("Settings"),
                    //    url: "Admin/HostSettings",
                    //    icon: "icon-settings",
                    //    requiredPermissionName: AppPermissions.Pages_Administration_Host_Settings
                    //    )
                    //)
                    //.AddItem(new MenuItemDefinition(
                    //    PageNames.App.Tenant.Settings,
                    //    L("Settings"),
                    //    url: "Admin/Settings",
                    //    icon: "icon-settings",
                    //    requiredPermissionName: AppPermissions.Pages_Administration_Tenant_Settings
                    //    )
                    //)
                ).AddItem(
                    new MenuItemDefinition(
                        PageNames.App.Tenant.NhanViens,
                        L("NhanVien"),
                        url: "Admin/NhanVien",
                        icon: "icon-user",
                        requiredPermissionName: AppPermissions.Pages_NhanVien_Read
                    )
                )
                .AddItem(
                    new MenuItemDefinition(
                        PageNames.App.Tenant.HoSos,
                        L("HoSo"),
                        icon: "icon-notebook",
                        requiredPermissionName: AppPermissions.Pages_HoSoTuThi_Read
                    ).AddItem(
                        new MenuItemDefinition(PageNames.App.Tenant.HoSoTuThis,
                        L("HoSoTuThi"),
                        url: "Admin/HoSo/TuThi",
                        icon: "icon-notebook",
                        requiredPermissionName: AppPermissions.Pages_HoSoTuThi_Read))
                        .AddItem(
                        new MenuItemDefinition(PageNames.App.Tenant.HoSoKhamTrinhs,
                        L("HoSoKhamTrinh"),
                        url: "Admin/HoSo/KhamTrinh",
                        icon: "icon-notebook",
                        requiredPermissionName: AppPermissions.Pages_HoSoKhamTrinh_Read))
                        .AddItem(
                        new MenuItemDefinition(PageNames.App.Tenant.HoSoThuongTats,
                        L("HoSoThuongTat"),
                        url: "Admin/HoSo/ThuongTat",
                        icon: "icon-notebook",
                        requiredPermissionName: AppPermissions.Pages_HoSoThuongTat_Read))


                ).AddItem(
                    new MenuItemDefinition(
                        PageNames.App.Tenant.BenhNhans,
                        L("BenhNhan"),
                        url: "Admin/BenhNhan",
                        icon: "icon-user-unfollow",
                        requiredPermissionName: AppPermissions.Pages_BenhNhan_Read
                    )
                ).AddItem(
                    new MenuItemDefinition(
                        PageNames.App.Tenant.TrinhDos,
                        L("TrinhDo"),
                        url: "Admin/TrinhDo",
                        icon: "icon-badge",
                        requiredPermissionName: AppPermissions.Pages_TrinhDo_Read
                    )
                ).AddItem(
                    new MenuItemDefinition(
                        PageNames.App.Tenant.ChucVus,
                        L("ChucVu"),
                        url: "Admin/ChucVu",
                        icon: "icon-tag",
                        requiredPermissionName: AppPermissions.Pages_ChucVu_Read
                    )
                );
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, PhapYConsts.LocalizationSourceName);
        }
    }
}