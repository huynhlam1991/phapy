﻿using System.Web.Optimization;
using TranLe.PhapY.Web.Bundling;

namespace TranLe.PhapY.Web.Areas.Admin.Startup
{
    public static class MpaBundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            //LIBRARIES

            AddMpaCssLibs(bundles, false);
            AddMpaCssLibs(bundles, true);
            bundles.Add(
                new ScriptBundle("~/Bundles/Admin/libs/js/jquery")
                    .Include(
                        ScriptPaths.JQuery
                        ).ForceOrdered()
                    );

            bundles.Add(
                new ScriptBundle("~/Bundles/Admin/libs/js")
                    .Include(
                        ScriptPaths.Json2,
                        ScriptPaths.JQuery_Migrate,
                        ScriptPaths.JQuery_UI,
                        ScriptPaths.Bootstrap,
                        ScriptPaths.Bootstrap_Hover_Dropdown,
                        ScriptPaths.JQuery_Slimscroll,
                        ScriptPaths.JQuery_BlockUi,
                        ScriptPaths.Js_Cookie,
                        ScriptPaths.JQuery_Uniform,
                        ScriptPaths.JQuery_Ajax_Form,
                        ScriptPaths.JQuery_jTable,
                        ScriptPaths.JQuery_jTable_RecordActions,
                        ScriptPaths.JQuery_Color,
                        ScriptPaths.JQuery_Jcrop,
                        ScriptPaths.JQuery_Timeago,
                        ScriptPaths.SignalR,
                        ScriptPaths.LocalForage,
                        ScriptPaths.Morris,
                        ScriptPaths.Morris_Raphael,
                        ScriptPaths.JQuery_Sparkline,
                        ScriptPaths.JsTree,
                        ScriptPaths.Bootstrap_Switch,
                        ScriptPaths.SpinJs,
                        ScriptPaths.SpinJs_JQuery,
                        ScriptPaths.PushJs,
                        ScriptPaths.SweetAlert,
                        ScriptPaths.Toastr,
                        ScriptPaths.MomentJs,
                        ScriptPaths.MomentTimezoneJs,
                        ScriptPaths.Underscore,
                        ScriptPaths.Abp,
                        ScriptPaths.Abp_JQuery,
                        ScriptPaths.Abp_Toastr,
                        ScriptPaths.Abp_BlockUi,
                        ScriptPaths.Abp_SpinJs,
                        ScriptPaths.Abp_SweetAlert,
                        ScriptPaths.Abp_Moment,
                        ScriptPaths.Abp_jTable,
                        ScriptPaths.MustacheJs,
                        ScriptPaths.Tether
                    ).ForceOrdered()
                );

            //COMMON (for MPA)
            bundles.Add(
                new ScriptBundle("~/Bundles/Admin/Common/js")
                    .IncludeDirectory("~/Areas/Admin/Common/Scripts", "*.js", true)
                    .Include("~/Areas/Admin/Views/Common/Modals/_LookupModal.js")
                    .ForceOrdered()
                );

            //METRONIC

            AddAppMetrinicCss(bundles, isRTL: false);
            AddAppMetrinicCss(bundles, isRTL: true);
            bundles.Add(
              new ScriptBundle("~/Bundles/Admin/metronic/js")
                  .Include(
                      "~/metronic/assets/global/scripts/app.js",
                      "~/metronic/assets/admin/layout4/scripts/layout.js",
                      "~/metronic/assets/layouts/global/scripts/quick-sidebar.js"
                  ).ForceOrdered()
              );

            // Kendo
            bundles.Add(
                new ScriptBundle("~/Bundles/js/kendo")
                    .Include(
                        "~/Scripts/kendo/2017.2.504/kendo.all.min.js",
                        "~/Scripts/kendo/2017.2.504/kendo.aspnetmvc.min.js",
                        "~/Scripts/kendo/2017.2.504/cultures/kendo.culture.vi-VN.min.js"
                    ).ForceOrdered()
            );
            bundles.Add(
                new ScriptBundle("~/Bundles/jqueryval")
                    .Include(
                        "~/Scripts/jquery.unobtrusive-ajax.min.js"
                    )
            );
        }

        private static void AddMpaCssLibs(BundleCollection bundles, bool isRTL)
        {
            bundles.Add(
                new StyleBundle("~/Bundles/Admin/libs/css" + (isRTL ? "RTL" : ""))
                    .Include(StylePaths.JQuery_UI, new CssRewriteUrlWithVirtualDirectoryTransform())
                    .Include(StylePaths.JQuery_jTable_Theme, new CssRewriteUrlWithVirtualDirectoryTransform())
                    .Include(StylePaths.FontAwesome, new CssRewriteUrlWithVirtualDirectoryTransform())
                    .Include(StylePaths.Simple_Line_Icons, new CssRewriteUrlWithVirtualDirectoryTransform())
                    .Include(StylePaths.FamFamFamFlags, new CssRewriteUrlWithVirtualDirectoryTransform())
                    .Include(isRTL ? StylePaths.BootstrapRTL : StylePaths.Bootstrap, new CssRewriteUrlWithVirtualDirectoryTransform())
                    .Include(StylePaths.JQuery_Uniform, new CssRewriteUrlWithVirtualDirectoryTransform())
                    .Include(StylePaths.JsTree, new CssRewriteUrlWithVirtualDirectoryTransform())
                    .Include(StylePaths.Morris)
                    .Include(StylePaths.Toastr)
                    .Include(StylePaths.Bootstrap_DateRangePicker)
                    .Include(StylePaths.Bootstrap_Switch)
                    //.Include(StylePaths.Bootstrap_Select)
                    .Include(StylePaths.JQuery_Jcrop)
                    .ForceOrdered()
                );
        }

        private static void AddAppMetrinicCss(BundleCollection bundles, bool isRTL)
        {
            bundles.Add(
                new StyleBundle("~/Bundles/Admin/metronic/css" + (isRTL ? "RTL" : ""))
                    .Include("~/fonts/material-icons/materialicons.css", new CssRewriteUrlTransform())
                    .Include("~/metronic/assets/global/css/components-md" + (isRTL ? "-rtl" : "") + ".min.css", new CssRewriteUrlWithVirtualDirectoryTransform())
                    .Include("~/metronic/assets/global/css/plugins-md" + (isRTL ? "-rtl" : "") + ".min.css", new CssRewriteUrlWithVirtualDirectoryTransform())
                    .Include("~/metronic/assets/admin/layout4/css/layout" + (isRTL ? "-rtl" : "") + ".min.css", new CssRewriteUrlWithVirtualDirectoryTransform())
                    .Include("~/metronic/assets/admin/layout4/css/themes/default" + (isRTL ? "-rtl" : "") + ".min.css", new CssRewriteUrlWithVirtualDirectoryTransform())
                    .Include("~/Themes/_all-themes" + (isRTL ? "-rtl" : "") + ".css", new CssRewriteUrlWithVirtualDirectoryTransform())
                    .ForceOrdered()
                );

            // Kendo
            bundles.Add(
                new StyleBundle("~/Bundles/Admin/kendo/css" + (isRTL ? "RTL" : ""))
                    .Include("~/Content/kendo/2017.2.504/kendo.common-bootstrap.min.css", new CssRewriteUrlTransform())
                    .Include("~/Content/kendo/2017.2.504/kendo.bootstrap.min.css", new CssRewriteUrlTransform())
                    .Include("~/Content/kendo/2017.2.504/kendo.bootstrap.mobile.min.css", new CssRewriteUrlTransform())
                    .ForceOrdered()
            );
        }
    }
}