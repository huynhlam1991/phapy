﻿function AddOrEditHoSoSuccess(res) {
    if (res && res.success) {
        if (res.type === "create") {
            abp.message.success("Thêm mới hồ sơ thành công.");
        } else {
            abp.message.success("Cập nhật hồ sơ thành công.");
        }
        var wnd = $("#HoSoPopup").data("kendoWindow");
        if (wnd) {
            wnd.close();
        }
    }
}
function AccountLoginEnd(args) {
    var id = $("#hdnbenhnhanId").val();
    if (args.type && args.type === "read") {
        if (args.response && args.response.length > 0) {
            var dropdownlist = $('#BenhNhanId').data("kendoDropDownList");
            if (dropdownlist) {
                dropdownlist.value(id);
            }
        }
    }
    
}
$(function () {
    $("#avatarUpload").change(function (e) {
        e.preventDefault();
        $("#livePreview").html("");
        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
        if (regex.test($(this).val().toLowerCase())) {
            if (typeof (FileReader) != "undefined") {
                $("#livePreview").show();
                $("#livePreview").append("<img />");
                var reader = new FileReader();
                reader.onload = function (e) {
                    $("#livePreview img").attr("src", e.target.result);
                }
                reader.readAsDataURL($(this)[0].files[0]);
            } else {
                alert("This browser does not support FileReader.");
            }
        } else {
            alert("Please upload a valid image file.");
        }
    });
});
$(function () {
    $('#form-hoso').ajaxForm({
        beforeSubmit: function (data) {
            abp.ui.setBusy();
            if (!$('#form-hoso').valid()) {
                abp.ui.clearBusy();
                return false;
            }
        },
        type: "POST",
        success: function (res) {
            if (res) {
                if (res.success) {
                    abp.message.success('Lưu hồ sơ thành công!', 'Đã Lưu Hồ Sơ')
                        .done(function () {
                            window.location.href = res.redirectTo;
                        });
                    
                } else {
                    abp.message.error(res.msg,'Không thể lưu hồ sơ');
                }
            }
            abp.ui.clearBusy();
            
        }
    });
});
$('.delete-image').click(function (e) {
    e.preventDefault();
    var obj = $(this);
    var id = obj.data('idimg');
    abp.message.confirm(
        'Bạn có chắc muốn xóa hình ảnh này khỏi hệ thống?',
        null,
        function(isConfirmed) {
            if (isConfirmed) {
                abp.ui.setBusy(
                    null,
                    abp.ajax({
                        contentType: 'application/json; charset=utf-8',
                        url: '/Admin/HoSo/DeleteImage',
                        data: JSON.stringify({ id: id })
                    }).done(function (result) {
                        if (result) {
                            if (result.success) {
                                abp.message.success('Xóa thành công', 'Đã xóa');
                                var item = obj.closest('.img-item');
                                item.remove();
                            } else {
                                abp.message.error(result.responseText, 'Không thể xóa');
                            }
                        }
                    })
                );
            }
        }
    );
});
$('.btn-print-select').click(function () {
    e.preventDefault();
    var obj = $(this);
    var id = obj.data('idimg');
    abp.message.confirm(
        'Bạn có chắc muốn lưu hình ảnh vào máy?',
        null,
        function (isConfirmed) {
            if (isConfirmed) {
                abp.ui.setBusy(
                    null,
                    abp.ajax({
                        contentType: 'application/json; charset=utf-8',
                        url: '/Admin/HoSo/DownloadImage',
                        data: JSON.stringify({ id: id })
                    }).done(function (result) {
                        if (result) {
                            if (result.success) {
                                abp.message.success('Thành công', 'Đã lưu');
                               
                            } else {
                                abp.message.error(result.responseText, 'Không thể lưu');
                            }
                        }
                    })
                );
            }
        }
    );
});
$(document).ready(function () {
    var customValidate = $("#form-hoso").kendoValidator().data("kendoValidator");
    $('input[name="NgaySinhBenhNhan"]').attr("data-val-date", "Ngày sinh không hợp lệ");
    var benhNhanId = $('#benhNhanId').val();
    if (Number(benhNhanId) == 0) {
        var option = {
            url: function () {
                return "/BenhNhan/GetBenhNhans";
            },
            preparePostData: function (data) {
                data.term = $("#HoTenBenhNhan").val();
                return data;
            },
            requestDelay: 500,
            getValue: "HoTen",
            ajaxSettings: {
                dataType: "json",
                method: "POST",
                data: {
                    dataType: "json"
                }
            },
            list: {
                match: {
                    enabled: true
                },
                onSelectItemEvent: function () {
                    debugger;
                    var data = $("#HoTenBenhNhan").getSelectedItemData();
                    var DiaChi = data.DiaChi;
                    var NgaySinh = new Date(parseInt(data.NamSinh.replace("/Date(", "").replace(")/", ""), 10));
                    var GioiTinh = data.GioiTinh;
                    var TrinhDoVanHoa = data.ThongTinCaNhan.TrinhDoVanHoa;
                    var NgheNghiep = data.ThongTinCaNhan.NgheNghiep;
                    var DienThoaiBenhNhan = data.SoDt;
                    var TonGiao = data.ThongTinCaNhan.TonGiao;
                    var DanToc = data.ThongTinCaNhan.DanToc;
                    var BenhNhanId = data.Id;
                    var QuocTich = data.ThongTinCaNhan.QuocTich;
                    $("#DiaChiBenhNhan").val(DiaChi);
                    $("#GioiTinhBenhNhan").val(GioiTinh).prop('checked', GioiTinh);
                    $("#ThongTinBenhNhan_NamSinh").data('kendoDatePicker').value(NgaySinh);
                    $("#TrinhDoVanHoaBenhNhan").val(TrinhDoVanHoa);
                    $("#NgheNghiepBenhNhan").val(NgheNghiep);
                    $("#benhNhanId").val(BenhNhanId);
                    $("#DienThoaiBenhNhan").val(DienThoaiBenhNhan);
                    $("#DanTocBenhNhan").val(DanToc);
                    $("#TonGiaoBenhNhan").val(TonGiao);
                    $("#QuocTichBenhNhan").val(QuocTich);
                    $('#imagePreview').attr('src', '/Data/BenhNhan/Avatar/' + data.ThongTinCaNhan.AvatarName);
                }
            }
        };
        $("#HoTenBenhNhan").easyAutocomplete(option);
    }
});
$('#btn-print-template-phu-luc').click(function () {
    var temp = $(this).parents('#schedule').find('.phulucWrapTemp').html();
    PopupCenterDual(temp, '', '', '640', '640');
});
$('#btn-add-template-phu-luc').click(function () {
    var id = $(this).data('id');
    var temp = $(this).parents('#schedule').find('.phulucWrapTemp').html();
    abp.ajax({
        contentType: 'application/json; charset=utf-8',
        url: '/Admin/HoSo/AddTempPhuLuc',
        data: JSON.stringify({ id: id, temp: temp })
    }).done(function (result) {
        if (result) {
            if (result.success) {
                abp.message.success('', 'Lưu thành công')
                    .done(function () {
                        //window.location.href = result.redirectTo;
                    });
            } else {
                abp.message.error(result.responseText, 'Không thể lưu');
            }
        }
    });
});

$('#btn-print-template-ho-so-duong-su').click(function () {
    var temp = $(this).parents('#hoSoDuongSu').find('.hoSoDuongSuWrapTemp').html();
    PopupCenterDual(temp, '', '', '620', '620');
});
$('#btn-add-template-ho-so-duong-su').click(function () {
    var id = $(this).data('id');
    var temp = $(this).parents('#hoSoDuongSu').find('.hoSoDuongSuWrapTemp').html();
    abp.ajax({
        contentType: 'application/json; charset=utf-8',
        url: '/Admin/HoSo/AddTempHoSoDuongSu',
        data: JSON.stringify({ id: id, temp: temp })
    }).done(function (result) {
        if (result) {
            if (result.success) {
                abp.message.success('', 'Lưu thành công')
                    .done(function () {
                    });
            } else {
                abp.message.error(result.responseText, 'Không thể lưu');
            }
        }
    });
});