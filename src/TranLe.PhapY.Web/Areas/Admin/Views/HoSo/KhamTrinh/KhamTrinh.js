﻿function onHoSoPopupClose(e) {
    var hoSoGrid = $('#HoSoGrid').data('kendoGrid');
    if (hoSoGrid) {
        hoSoGrid.dataSource.read();
    }
    e.sender.content('');
}
function onHoSoPopupOpen(w) {
    if (w.sender) {
        kendo.ui.progress(w.sender.element, true);
    }
}

function deleteHoSo(e) {
    e.preventDefault();
    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
    abp.message.confirm(
        'Bạn có chắc muốn xóa thông tin hồ sơ này khỏi hệ thống?',
        null,
        function (isConfirmed) {
            if (isConfirmed) {
                if (dataItem.Id) {
                    abp.ui.setBusy(
                        null,
                        abp.ajax({
                            contentType: 'application/json; charset=utf-8',
                            url: '/HoSo/DeleteKhamTrinh',
                            data: JSON.stringify({ id: dataItem.Id })
                        }).done(function (result) {
                            if (result) {
                                if (result.success) {
                                    abp.message.success('Xóa hồ sơ thành công','Đã xóa hồ sơ');
                                    var hoSoGrid = $('#hosoGrid').data('kendoGrid');
                                    if (hoSoGrid) {
                                        hoSoGrid.dataSource.read();
                                    }
                                }
                                else {
                                    abp.message.error(result.responseText, 'Không thể xóa hồ sơ');
                                    var hoSoGrid = $('#hosoGrid').data('kendoGrid');
                                    if (hoSoGrid) {
                                        hoSoGrid.dataSource.read();
                                    }
                                }
                            }
                        })
                    );
                }
            }
        }
    );
}