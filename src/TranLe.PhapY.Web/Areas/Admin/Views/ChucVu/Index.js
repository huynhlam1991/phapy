﻿function onChucVuPopupClose(e) {
    var chucVuGrid = $('#chucvuGrid').data('kendoGrid');
    if (chucVuGrid) {
        chucVuGrid.dataSource.read();
    }
    e.sender.content('');
}
function onChucVuPopupOpen(w) {
    if (w.sender) {
        kendo.ui.progress(w.sender.element, true);
    }
}
function editChucVu(e) {
    e.preventDefault();
    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));

    var wnd = $("#ChucVuPopup").data("kendoWindow");
    if (wnd) {
        wnd.refresh({
            url: "/ChucVu/Edit/" + dataItem.Id
        });
        wnd.title("Cập nhật thông tin chức vụ");
        wnd.center().open();
        wnd.maximize();
    }
}
function deleteChucVu(e) {
    e.preventDefault();
    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
    abp.message.confirm(
        'Bạn có chắc muốn xóa chức vụ này khỏi hệ thống?',
        'Bạn muốn xóa?',
        function (isConfirmed) {
            if (isConfirmed) {
                if (dataItem.Id) {
                    abp.ui.setBusy(
                        null,
                        abp.ajax({
                            contentType: 'application/json; charset=utf-8',
                            url: '/ChucVu/Delete',
                            data: JSON.stringify({ id: dataItem.Id })
                        }).done(function (result) {
                            if (result) {
                                if (result.success) {
                                    abp.notify.success('Xóa chức vụ thành công');
                                    var chucVuGrid = $('#chucvuGrid').data('kendoGrid');
                                    if (chucVuGrid) {
                                        chucVuGrid.dataSource.read();
                                    }
                                }
                            }
                        })
                    );
                }
            }
        }
    );
}