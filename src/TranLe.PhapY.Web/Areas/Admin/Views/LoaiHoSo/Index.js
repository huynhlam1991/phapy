﻿function onLoaiHoSoPopupClose(e) {
    var loaiHoSoGrid = $('#loaihosoGrid').data('kendoGrid');
    if (loaiHoSoGrid) {
        loaiHoSoGrid.dataSource.read();
    }
    e.sender.content('');
}
function onLoaiHoSoPopupOpen(w) {
    if (w.sender) {
        kendo.ui.progress(w.sender.element, true);
    }
}
function editLoaiHoSo(e) {
    e.preventDefault();
    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));

    var wnd = $("#LoaiHoSoPopup").data("kendoWindow");
    if (wnd) {
        wnd.refresh({
            url: "/LoaiHoSo/Edit/" + dataItem.Id
        });
        wnd.title("Cập nhật thông tin loại hồ sơ");
        wnd.center().open();
        wnd.maximize();
    }
}
function deleteLoaiHoSo(e) {
    e.preventDefault();
    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
    abp.message.confirm(
        'Bạn có chắc muốn xóa loại hồ sơ này khỏi hệ thống?',
        null,
        function (isConfirmed) {
            if (isConfirmed) {
                if (dataItem.Id) {
                    abp.ui.setBusy(
                        null,
                        abp.ajax({
                            contentType: 'application/json; charset=utf-8',
                            url: '/LoaiHoSo/Delete',
                            data: JSON.stringify({ id: dataItem.Id })
                        }).done(function (result) {
                            if (result) {
                                if (result.success) {
                                    abp.notify.success('Xóa loại hồ sơ thành công');
                                    var loaiHoSoGrid = $('#loaihosoGrid').data('kendoGrid');
                                    if (loaiHoSoGrid) {
                                        loaiHoSoGrid.dataSource.read();
                                    }
                                }
                            }
                        })
                    );
                }
            }
        }
    );
}