﻿var customValidate = $("#form-loaihoso").kendoValidator().data("kendoValicustomValidatedator");
function AddOrEditLoaiHoSoSuccess(res) {
    if (res && res.success) {
        if (res.type === "create") {
            abp.message.success("Thêm mới loại hồ sơ thành công.");
        } else {
            abp.message.success("Cập nhật loại hồ sơ thành công.");
        }
        var wnd = $("#LoaiHoSoPopup").data("kendoWindow");
        if (wnd) {
            wnd.close();
        }
    }
}
function AddOrEditLoaiHoSoBegin() {

}