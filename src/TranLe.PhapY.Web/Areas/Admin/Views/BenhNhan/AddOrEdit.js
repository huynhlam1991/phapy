﻿var customValidate = $("#form-benhnhan").kendoValidator().data("kendoValidator");
function AddOrEditBenhNhanSuccess(res) {
    if (res && res.success) {
        if (res.type === "create") {
            abp.message.success("Thêm mới thông tin bệnh nhân thành công.");
        } else {
            abp.message.success("Cập nhật thông tin bệnh nhân thành công.");
        }
        var wnd = $("#BenhNhanPopup").data("kendoWindow");
        if (wnd) {
            wnd.close();
        }
    }
}
function AddOrEditBenhNhanBegin() {

}
//function AccountLoginEnd(args) {
//    var id = $("#hdnAccountLoginId").val();
//    if (args.type && args.type === "read") {
//        if (args.response && args.response.length > 0) {
//            var dropdownlist = $('#UserId').data("kendoDropDownList");
//            if (dropdownlist) {
//                dropdownlist.value(id);
//            }
//        }
//    }
//}
$(document).ready(function () {
    $('input[name="NgaySinh"]').attr("data-val-date", "Ngày sinh không hợp lệ");
});
$(function () {
    $('#form-benhnhan').ajaxForm({
        beforeSubmit: function (data) {
            abp.ui.setBusy();
            if (!$('#form-benhnhan').valid()) {
                abp.ui.clearBusy();
                return false;
            }
        },
        type: "POST",
        success: function (res) {
            if (res) {
                if (res.success) {
                    var wnd = $("#BenhNhanPopup").data("kendoWindow");
                    if (wnd) {
                        wnd.close();
                        abp.message.success('Cập nhật bệnh nhân thành công!', 'Đã Cập Nhật Bệnh Nhân')
                            .done(function () {

                            });
                    }
                } else {
                    abp.notify.error(res.msg, 'Không thể cập nhật bệnh nhân');
                }
            }
            abp.ui.clearBusy();

        }
    });
});