﻿function onBenhNhanPopupClose(e) {
    var benhNhanGrid = $('#benhnhanGrid').data('kendoGrid');
    if (benhNhanGrid) {
        benhNhanGrid.dataSource.read();
    }
    e.sender.content('');
}
function onBenhNhanPopupOpen(w) {
    if (w.sender) {
        kendo.ui.progress(w.sender.element, true);
    }
}
function editBenhNhan(e) {
    e.preventDefault();
    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));

    var wnd = $("#BenhNhanPopup").data("kendoWindow");
    if (wnd) {
        wnd.refresh({
            url: "/BenhNhan/Edit/" + dataItem.Id
        });
        wnd.title("Cập nhật thông tin bệnh nhân");
        wnd.center().open();
        wnd.maximize();
    }
}
function deleteBenhNhan(e) {
    e.preventDefault();
    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
    abp.message.confirm(
        'Bạn có chắc muốn xóa thông tin bệnh nhân này khỏi hệ thống?',
        null,
        function (isConfirmed) {
            if (isConfirmed) {
                if (dataItem.Id) {
                    abp.ui.setBusy(
                        null,
                        abp.ajax({
                            contentType: 'application/json; charset=utf-8',
                            url: '/BenhNhan/Delete',
                            data: JSON.stringify({ id: dataItem.Id })
                        }).done(function (result) {
                            if (result) {
                                if (result.success) {
                                    abp.notify.success('Xóa bệnh nhân thành công');
                                    var benhNhanGrid = $('#benhnhanGrid').data('kendoGrid');
                                    if (benhNhanGrid) {
                                        benhNhanGrid.dataSource.read();
                                    }
                                }
                            }
                        })
                    );
                }
            }
        }
    );
}