﻿function onTrinhDoPopupClose(e) {
    var trinhDoGrid = $('#trinhdoGrid').data('kendoGrid');
    if (trinhDoGrid) {
        trinhDoGrid.dataSource.read();
    }
    e.sender.content('');
}
function onTrinhDoPopupOpen(w) {
    if (w.sender) {
        kendo.ui.progress(w.sender.element, true);
    }
}
function editTrinhDo(e) {
    e.preventDefault();
    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));

    var wnd = $("#TrinhDoPopup").data("kendoWindow");
    if (wnd) {
        wnd.refresh({
            url: "/TrinhDo/Edit/" + dataItem.Id
        });
        wnd.title("Cập nhật thông tin trình độ");
        wnd.center().open();
        wnd.maximize();
    }
}
function deleteTrinhDo(e) {
    e.preventDefault();
    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
    abp.message.confirm(
        'Bạn có chắc muốn xóa trình độ này khỏi hệ thống?',
        null,
        function (isConfirmed) {
            if (isConfirmed) {
                if (dataItem.Id) {
                    abp.ui.setBusy(
                        null,
                        abp.ajax({
                            contentType: 'application/json; charset=utf-8',
                            url: '/TrinhDo/Delete',
                            data: JSON.stringify({ id: dataItem.Id })
                        }).done(function (result) {
                            if (result) {
                                if (result.success) {
                                    abp.notify.success('Xóa trình độ thành công');
                                    var trinhDoGrid = $('#trinhdoGrid').data('kendoGrid');
                                    if (trinhDoGrid) {
                                        trinhDoGrid.dataSource.read();
                                    }
                                }
                            }
                        })
                    );
                }
            }
        }
    );
}