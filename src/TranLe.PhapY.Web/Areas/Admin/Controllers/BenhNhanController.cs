﻿using Abp.Domain.Repositories;
using Abp.ObjectMapping;
using Abp.Web.Mvc.Authorization;
using System;
using System.Configuration;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Abp.Web.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Abp.Domain.Entities;
using Abp.Runtime.Validation;
using TranLe.PhapY.Models;
using TranLe.PhapY.Authorization;
using TranLe.PhapY.BenhNhans.Dto;
using TranLe.PhapY.Web.Helpers;

namespace TranLe.PhapY.Web.Areas.Admin.Controllers
{
    [AbpMvcAuthorize]
    public class BenhNhanController : PhapYControllerBase
    {
        private string _pathBenhNhanAvatar;
        public string PathBenhNhanAvatar
        {
            get
            {
                _pathBenhNhanAvatar = ConfigurationManager.AppSettings["BenhNhanAvaPath"];
                if (_pathBenhNhanAvatar == null)
                    return "/Data/BenhNhan/";
                return _pathBenhNhanAvatar;
            }
        }
        private readonly IRepository<BenhNhan> _benhnhanRepository;
        private readonly IRepository<HoSo> _hosoRepository;
        private readonly IObjectMapper _objectMapper;
        public BenhNhanController(IRepository<BenhNhan> benhnhanRepository, IRepository<HoSo> hosoRepository, IObjectMapper objectMapper)
        {
            _benhnhanRepository = benhnhanRepository;
            _hosoRepository = hosoRepository;
            _objectMapper = objectMapper;
        }
        [AbpMvcAuthorize(AppPermissions.Pages_BenhNhan_Read)]
        public ActionResult Index()
        {
            return View();
        }


        [AbpMvcAuthorize(AppPermissions.Pages_BenhNhan_Read)]
        [WrapResult(WrapOnSuccess = false, WrapOnError = true)]
        public async Task<ActionResult> GetBenhNhans(string term)
        {
            var data = await _benhnhanRepository.GetAll()
                .Where(x => x.HoTen.Contains(term))
                .ToListAsync();
            var results = data.Select(x => new BenhNhanDto()
            {
                HoTen = x.HoTen,
                DiaChi = x.DiaChi,
                GioiTinh = x.GioiTinh,
                NamSinh = x.NamSinh,
                Id = x.Id,
                SoDt = x.SoDt,
                ThongTinCaNhan = x.GetData<ThongTinCaNhan>("ThongTinCaNhan")
            });
            return Json(results, JsonRequestBehavior.AllowGet);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_BenhNhan_Read)]
        [WrapResult(WrapOnSuccess = false, WrapOnError = true)]
        public async Task<ActionResult> ListBenhNhans([DataSourceRequest] DataSourceRequest request)
        {
            var data = await _benhnhanRepository.GetAll()
                .Select(x => new BenhNhanDto
                {
                    Id = x.Id,
                    HoTen = x.HoTen,
                    NamSinh = x.NamSinh,
                    DiaChi = x.DiaChi,
                    SoDt = x.SoDt,
                    GioiTinh = x.GioiTinh
                })
                .ToListAsync();
            return Json(data.ToDataSourceResult(request));
        }

        public ActionResult GetBenhNhan(int id)
        {
            var data = _benhnhanRepository.FirstOrDefault(x => x.Id == id);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_BenhNhan_Create)]
        public ActionResult Add()
        {
            var model = new BenhNhanDto()
            {
                NamSinh = DateTime.Now.AddYears(-20)
            };

            return PartialView("~/Areas/Admin/Views/BenhNhan/Add.cshtml", model);
        }
        [HttpPost]
        [AbpMvcAuthorize(AppPermissions.Pages_BenhNhan_Create)]
        [DisableValidation]
        [WrapResult(WrapOnSuccess = false, WrapOnError = true)]
        public async Task<JsonResult> Add(BenhNhanDto model)
        {
            var msg = string.Empty;
            try
            {
                if (ModelState.IsValid)
                {
                    var benhNhan = _objectMapper.Map<BenhNhan>(model);
                    var image = Request.Files;
                    string avaName = string.Empty;
                    if (image.Count > 0)
                    {
                        avaName = image.Count > 0 ? Path.GetFileNameWithoutExtension(image[0].FileName) + "_" + Guid.NewGuid().ToString().Substring(0, 4)
                                                    + Path.GetExtension(image[0].FileName) : "";
                    }
                    ThongTinCaNhan thongTinCaNhan = new ThongTinCaNhan()
                    {
                        DanToc = model.ThongTinCaNhan.DanToc,
                        TonGiao = model.ThongTinCaNhan.TonGiao,
                        NgheNghiep = model.ThongTinCaNhan.NgheNghiep,
                        QuocTich = model.ThongTinCaNhan.QuocTich,
                        TrinhDoVanHoa = model.ThongTinCaNhan.TrinhDoVanHoa,
                        AvatarName = avaName
                    };
                    benhNhan.SetData("ThongTinCaNhan", thongTinCaNhan);
                    //model.ThongTinCaNhan  = benhNhan.GetData<ThongTinCaNhan>("ThongTinCaNhan");
                    using (var unitOfWork = UnitOfWorkManager.Begin())
                    {
                        await _benhnhanRepository.InsertAsync(benhNhan);
                        await unitOfWork.CompleteAsync();
                        if (image.Count > 0)
                        {
                            var path = Server.MapPath(PathBenhNhanAvatar);
                            DirectoryHelpers.CreateDirectory(path);
                            image[0].SaveAs(path + avaName);
                        }
                        return Json(new
                        {
                            success = true,
                            type = "create",
                            responseText = "success"
                        }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                msg = e.Message;
            }
            return Json(new
            {
                success = false,
                responseText = msg
            }, JsonRequestBehavior.AllowGet);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_BenhNhan_Update)]
        public ActionResult Edit(int id)
        {
            var benhNhan = _benhnhanRepository.FirstOrDefault(x => x.Id.Equals(id));
            var model = new BenhNhanDto();
            if (benhNhan != null)
            {
                model.Id = benhNhan.Id;
                model.HoTen = benhNhan.HoTen;
                model.GioiTinh = benhNhan.GioiTinh;
                model.NamSinh = benhNhan.NamSinh;
                model.SoDt = benhNhan.SoDt;
                model.ThongTinCaNhan = benhNhan.GetData<ThongTinCaNhan>("ThongTinCaNhan");
                model.DiaChi = benhNhan.DiaChi;
            }
            return PartialView("~/Areas/Admin/Views/BenhNhan/Edit.cshtml", model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_BenhNhan_Update)]
        [HttpPost]
        [WrapResult(WrapOnSuccess = false, WrapOnError = true)]
        [DisableValidation]
        public async Task<JsonResult> Edit(BenhNhanDto model)
        {
            var msg = string.Empty;
            try
            {
                if (ModelState.IsValid)
                {
                    var benhNhan = _objectMapper.Map<BenhNhan>(model);

                    using (var unitOfWork = UnitOfWorkManager.Begin())
                    {
                        var image = Request.Files;
                        
                        
                        ThongTinCaNhan thongTinCaNhan = new ThongTinCaNhan()
                        {
                            DanToc = model.ThongTinCaNhan.DanToc,
                            TonGiao = model.ThongTinCaNhan.TonGiao,
                            NgheNghiep = model.ThongTinCaNhan.NgheNghiep,
                            QuocTich = model.ThongTinCaNhan.QuocTich,
                            TrinhDoVanHoa = model.ThongTinCaNhan.TrinhDoVanHoa,
                            AvatarName = model.ThongTinCaNhan.AvatarName
                        };
                        if (image.Count > 0)
                        {
                            string avaName = string.Empty;
                            avaName = image.Count > 0 ? Path.GetFileNameWithoutExtension(image[0].FileName) + "_" + Guid.NewGuid().ToString().Substring(0, 4)
                                                        + Path.GetExtension(image[0].FileName) : "";
                            thongTinCaNhan.AvatarName = avaName;
                            var path = Server.MapPath(PathBenhNhanAvatar);
                            DirectoryHelpers.CreateDirectory(path);
                            image[0].SaveAs(path + avaName);
                        }
                        benhNhan.SetData("ThongTinCaNhan", thongTinCaNhan);
                        await _benhnhanRepository.UpdateAsync(benhNhan);

                        await unitOfWork.CompleteAsync();
                        return Json(new
                        {
                            success = true,
                            type = "update",
                            responseText = "success"
                        }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                msg = e.Message;
            }
            return Json(new
            {
                success = false,
                responseText = msg
            }, JsonRequestBehavior.AllowGet);
        }

        [WrapResult(WrapOnSuccess = false, WrapOnError = true)]
        [AcceptVerbs(HttpVerbs.Post)]
        [AbpMvcAuthorize(AppPermissions.Pages_BenhNhan_Delete)]
        public async Task<ActionResult> Delete([DataSourceRequest] DataSourceRequest request, int id)
        {
            using (var uniOfWork = UnitOfWorkManager.Begin())
            {
                if (id != 0)
                {
                    int hs = _hosoRepository.Count(x => x.BenhNhanId == id);
                    if (hs ==0)
                    {
                        var benhNhan = await _benhnhanRepository.FirstOrDefaultAsync(x => x.Id == id);
                        if (benhNhan != null)
                        {
                            await _benhnhanRepository.DeleteAsync(benhNhan);
                            await uniOfWork.CompleteAsync();
                            return Json(new
                            {
                                success = true,
                                responseText = "success"
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    return Json(new
                    {
                        success = false,
                        responseText = "fail because this patient have records"
                    }, JsonRequestBehavior.AllowGet);
                }

                return Json(new
                {
                    success = false,
                    responseText = "fail"
                }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}