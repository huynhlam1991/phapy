using System.Web.Mvc;
using Abp.Web.Mvc.Authorization;
using TranLe.PhapY.Web.Controllers;

namespace TranLe.PhapY.Web.Areas.Admin.Controllers
{
    [AbpMvcAuthorize]
    public class WelcomeController : PhapYControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}