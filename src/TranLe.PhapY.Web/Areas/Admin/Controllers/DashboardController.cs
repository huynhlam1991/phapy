﻿using System.Web.Mvc;
using Abp.Web.Mvc.Authorization;
using TranLe.PhapY.Authorization;
using TranLe.PhapY.Web.Controllers;

namespace TranLe.PhapY.Web.Areas.Admin.Controllers
{
    [AbpMvcAuthorize(AppPermissions.Pages_Tenant_Dashboard)]
    public class DashboardController : PhapYControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}