﻿using Abp.Domain.Entities;
using Abp.Domain.Repositories;
using Abp.ObjectMapping;
using Abp.Web.Models;
using Abp.Web.Mvc.Authorization;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using TranLe.PhapY.Authorization;
using System;
using System.Configuration;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Abp.IO;
using TranLe.PhapY.BenhNhans.Dto;
using TranLe.PhapY.HoSos.Dto;
using TranLe.PhapY.Models;
using System.Collections.Generic;
using System.IO.Compression;
using System.Text;
using System.Text.RegularExpressions;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using Aspose.Words;
using TranLe.PhapY.Web.Helpers;

namespace TranLe.PhapY.Web.Areas.Admin.Controllers
{
    [AbpMvcAuthorize]
    public class HoSoController : PhapYControllerBase
    {
        private readonly IRepository<HoSo> _hosoRepository;
        private readonly IRepository<NhanVien> _nhanvienRepository;
        private readonly IRepository<ChucVu> _chucvuRepository;
        private readonly IRepository<BenhNhan> _benhnhanRepository;
        private readonly IObjectMapper _objectMapper;
        private readonly IRepository<HinhAnh> _hinhAnhRepository;
        private string _pathHoSoImage;
        private string _pathBenhNhanAvatar;

        public string PathHoSoImage
        {
            get
            {
                _pathHoSoImage = ConfigurationManager.AppSettings["HoSoImagePath"];
                if (_pathHoSoImage == null)
                    return "/Data/HoSo/";
                return _pathHoSoImage;
            }
        }
        public string PathBenhNhanAvatar
        {
            get
            {
                _pathBenhNhanAvatar = ConfigurationManager.AppSettings["BenhNhanAvaPath"];
                if (_pathBenhNhanAvatar == null)
                    return "/Data/BenhNhan/";
                return _pathBenhNhanAvatar;
            }
        }
        private string _pathDefaultPrintTuThi;

        public string PathDefaultPrintTuThi
        {
            get
            {
                _pathDefaultPrintTuThi = ConfigurationManager.AppSettings["HoSoTemplatePrintTuThiPath"];
                if (_pathDefaultPrintTuThi == null)
                    return "/App_Data/HoSo/Templates/TuThi.docx";
                return _pathDefaultPrintTuThi;
            }
        }

        private string _pathDefaultPrintKhamTrinh;

        public string PathDefaultPrintKhamTrinh
        {
            get
            {
                _pathDefaultPrintKhamTrinh = ConfigurationManager.AppSettings["HoSoTemplatePrintKhamTrinhPath"];
                if (_pathDefaultPrintKhamTrinh == null)
                    return "/App_Data/HoSo/Templates/KhamTrinh.docx";
                return _pathDefaultPrintKhamTrinh;
            }
        }
        private string _pathDefaultPrintThuongTat;

        public string PathDefaultPrintThuongTat
        {
            get
            {
                _pathDefaultPrintThuongTat = ConfigurationManager.AppSettings["HoSoTemplatePrintTuThiPath"];
                if (_pathDefaultPrintThuongTat == null)
                    return "/App_Data/HoSo/Templates/ThuongTat.docx";
                return _pathDefaultPrintThuongTat;
            }
        }

        private string _pathTemplateTuThi;

        public string PathTemplateTuThi
        {
            get
            {
                _pathTemplateTuThi = ConfigurationManager.AppSettings["PathTemplateTuThi"];
                if (_pathTemplateTuThi == null)
                    return "/App_Data/HoSo/Templates/TemplateContentTuThi.docx";
                return _pathTemplateTuThi;
            }
        }
        private string _pathTemplateKhamTrinh;

        public string PathTemplateKhamTrinh
        {
            get
            {
                _pathTemplateKhamTrinh = ConfigurationManager.AppSettings["PathTemplateKhamTrinh"];
                if (_pathTemplateKhamTrinh == null)
                    return "/App_Data/HoSo/Templates/TemplateContentKhamTrinh.docx";
                return _pathTemplateKhamTrinh;
            }
        }
        private string _pathTemplateThuongTat;

        public string PathTemplateThuongTat
        {
            get
            {
                _pathTemplateThuongTat = ConfigurationManager.AppSettings["PathTemplateThuongTat"];
                if (_pathTemplateThuongTat == null)
                    return "/App_Data/HoSo/Templates/TemplateContentThuongTat.docx";
                return _pathTemplateThuongTat;
            }
        }
        private string _pathTemp;

        public string PathTemp
        {
            get
            {
                _pathTemp = ConfigurationManager.AppSettings["PathTemp"];
                if (_pathTemp == null)
                    return "/App_Data/Temp/";
                return _pathTemp;
            }
        }
        private string _hoSoDataPath;

        public string HoSoDataPath
        {
            get
            {
                _hoSoDataPath = ConfigurationManager.AppSettings["HoSoDataPath"];
                if (_hoSoDataPath == null)
                    return "~/App_Data/HoSo/";
                return _hoSoDataPath;
            }
        }

        private string _hoSoTemplatePrintTuThiPath;

        public string HoSoTemplatePrintTuThiPath
        {
            get
            {
                _hoSoTemplatePrintTuThiPath = ConfigurationManager.AppSettings["HoSoTemplatePrintTuThiPath"];
                if (_hoSoTemplatePrintTuThiPath == null)
                    return "/App_Data/DefaultPrintTuThi.docx";
                return _hoSoTemplatePrintTuThiPath;
            }
        }
        public HoSoController(IRepository<HoSo> hosoRepository,
            IRepository<NhanVien> nhanvienRepository,
            IRepository<ChucVu> chucvuRepository,
            IRepository<BenhNhan> benhnhanRepository,
            IObjectMapper objectMapper,
            IRepository<HinhAnh> hinhAnhRepository)
        {
            _hosoRepository = hosoRepository;
            _nhanvienRepository = nhanvienRepository;
            _chucvuRepository = chucvuRepository;
            _benhnhanRepository = benhnhanRepository;
            _objectMapper = objectMapper;
            _hinhAnhRepository = hinhAnhRepository;
        }
        [AbpMvcAuthorize(AppPermissions.Pages_HoSoTuThi_Read)]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult RenderRichTextEditor(string filePath)
        {
            return PartialView("~/Areas/Admin/Views/HoSo/_RichTextEditorPartial.cshtml", filePath);
        }
        #region TuThi

        [AbpMvcAuthorize(AppPermissions.Pages_HoSoTuThi_Read)]
        public ActionResult TuThi()
        {
            return View("TuThi/TuThi");
        }
        [AbpMvcAuthorize(AppPermissions.Pages_HoSoTuThi_Create)]
        public ActionResult AddTuThi()
        {
            int type = 1;
            var path = CreateFileTempAddHoSo(type);
            var model = new HoSoTuThiDto
            {
                IsHaveContent = false,
                ContentFileName = path,
                NgayGiamDinh = DateTime.Now.Date,
                NgayQuyetDinhTrungCau = DateTime.Now.Date,
                LoaiHoSoId = type,
                ThongTinBenhNhan = new BenhNhanDto
                {
                    Id = 0,
                    NamSinh = DateTime.Now.AddYears(-20).Date
                }
            };
            return PartialView("~/Areas/Admin/Views/HoSo/TuThi/AddTuThi.cshtml", model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_HoSoTuThi_Update)]
        public ActionResult EditTuThi(int id)
        {
            ViewBag.HoSoPath = HoSoDataPath;
            var hoSo = _hosoRepository.GetAll().FirstOrDefault(x => x.Id.Equals(id));
            var benhNhan = _benhnhanRepository.FirstOrDefault(x => x.Id.Equals(hoSo.BenhNhanId));
            var model = hoSo.GetTuThiDto(benhNhan);
            ViewBag.NhanViens = hoSo.NhanViens.Select(x => new CheckBoxViewModel()
            {
                NhanVienId = x.Id,
                TenNhanVien = x.HoTen,
                TenChucVu = x.ChucVu.TenChucVu
            }).ToList();
            return PartialView("~/Areas/Admin/Views/HoSo/TuThi/EditTuThi.cshtml", model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_HoSoTuThi_Update)]
        [HttpPost]
        [WrapResult(WrapOnSuccess = false, WrapOnError = true)]
        public async Task<JsonResult> EditTuThi(HoSoTuThiDto model)
        {

            var msg = string.Empty;
            try
            {
                if (ModelState.IsValid)
                {
                    using (var unitOfWork = UnitOfWorkManager.Begin())
                    {
                        var hoSo = await _hosoRepository.GetAll().Include(x => x.NhanViens)
                            .Where(x => x.Id == model.Id)
                            .FirstOrDefaultAsync();
                        if (hoSo != null)
                        {
                            var oldNhanViens = hoSo.NhanViens.ToList();
                            var imageAva = Request.Files;
                            if (imageAva.Count > 0)
                            {
                                var benhnhan = _benhnhanRepository.FirstOrDefault(x => x.Id == model.ThongTinBenhNhan.Id);
                                if (benhnhan != null)
                                {
                                    var thongtinBenhNhan = benhnhan.GetData<ThongTinCaNhan>("ThongTinCaNhan");
                                    if (thongtinBenhNhan != null)
                                    {
                                        var path = PathBenhNhanAvatar + thongtinBenhNhan.AvatarName;
                                        if (!string.IsNullOrEmpty(thongtinBenhNhan.AvatarName))
                                        {
                                            DirectoryHelpers.DeleteFile(Server.MapPath(path));
                                        }
                                        var newAva = string.Empty;
                                        newAva = imageAva.Count > 0 ? Path.GetFileNameWithoutExtension(imageAva[0].FileName) + "_" + Guid.NewGuid().ToString().Substring(0, 4)
                                                                    + Path.GetExtension(imageAva[0].FileName) : "";
                                        thongtinBenhNhan.AvatarName = newAva;
                                        benhnhan.SetData("ThongTinCaNhan", thongtinBenhNhan);
                                        var pathImageAva = Server.MapPath(PathBenhNhanAvatar);
                                        DirectoryHelpers.CreateDirectory(pathImageAva);
                                        imageAva[0].SaveAs(pathImageAva + newAva);
                                        CurrentUnitOfWork.SaveChanges();
                                    }
                                }
                            }

                            model.MapTo(hoSo);
                            hoSo.NhanViens = oldNhanViens;
                            // remove old nhanviens
                            foreach (var item in oldNhanViens.ToList())
                            {
                                hoSo.NhanViens.Remove(item);
                            }
                            // add new nhanviens
                            var nhanViens = await _nhanvienRepository.GetAll().ToListAsync();
                            if (model.NhanVienIds != null && model.NhanVienIds.Any())
                            {
                                foreach (var item in model.NhanVienIds)
                                {
                                    var nhanVien = nhanViens.FirstOrDefault(x => x.Id == item);
                                    if (nhanVien != null)
                                    {
                                        hoSo.NhanViens.Add(nhanVien);
                                    }
                                }
                            }

                            hoSo.SetData("BacSiGiamDinh", model.BacSiGiamDinh);
                            hoSo.SetData("NguyenNhanXayRa", model.NguyenNhanXayRa);
                            await _hosoRepository.UpdateAsync(hoSo);
                            if (model.HoSoImageDtos != null && model.HoSoImageDtos.Any())
                            {
                                foreach (var item in model.HoSoImageDtos)
                                {
                                    var image = await _hinhAnhRepository.FirstOrDefaultAsync(x => x.Id.Equals(item.Id));
                                    if (image != null)
                                    {
                                        image.DuocChon = item.DuocChon;
                                        image.ThuTuChon = item.ThuTuChon;
                                        await _hinhAnhRepository.UpdateAsync(image);
                                    }
                                }
                            }

                            await unitOfWork.CompleteAsync();
                            return Json(new
                            {
                                success = true,
                                type = "update",
                                redirectTo = Url.Action("TuThi", "HoSo"),
                                responseText = "success"
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                msg = e.Message;
            }

            return Json(new
            {
                success = false,
                responseText = msg
            }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [AbpMvcAuthorize(AppPermissions.Pages_HoSoTuThi_Create)]
        [WrapResult(WrapOnSuccess = false, WrapOnError = true)]
        public async Task<JsonResult> AddTuThi(HoSoTuThiDto model)
        {
            var msg = string.Empty;

            try
            {
                using (var unitOfWork = UnitOfWorkManager.Begin())
                {
                    if (ModelState.IsValid)
                    {
                        var image = Request.Files;
                        var benhNhan = _benhnhanRepository.GetAll()
                            .FirstOrDefault(x => x.Id == model.ThongTinBenhNhan.Id);
                        var avaName = string.Empty;
                        if (benhNhan == null)
                        {
                            benhNhan = _objectMapper.Map<BenhNhan>(model.ThongTinBenhNhan);

                            avaName = image.Count > 0 ? Path.GetFileNameWithoutExtension(image[0].FileName) + "_" + Guid.NewGuid().ToString().Substring(0, 4)
                                                        + Path.GetExtension(image[0].FileName) : "";
                            var thongTinCaNhan = new ThongTinCaNhan()
                            {
                                DanToc = model.ThongTinBenhNhan.ThongTinCaNhan.DanToc,
                                TonGiao = model.ThongTinBenhNhan.ThongTinCaNhan.TonGiao,
                                NgheNghiep = model.ThongTinBenhNhan.ThongTinCaNhan.NgheNghiep,
                                QuocTich = model.ThongTinBenhNhan.ThongTinCaNhan.QuocTich,
                                TrinhDoVanHoa = model.ThongTinBenhNhan.ThongTinCaNhan.TrinhDoVanHoa,
                                AvatarName = avaName
                            };
                            benhNhan.SetData("ThongTinCaNhan", thongTinCaNhan);
                            benhNhan.Id = await _benhnhanRepository.InsertAndGetIdAsync(benhNhan);
                            CurrentUnitOfWork.SaveChanges();
                        }
                        if (image.Count > 0)
                        {
                            var path = Server.MapPath(PathBenhNhanAvatar);
                            DirectoryHelpers.CreateDirectory(path);
                            image[0].SaveAs(path + avaName);
                        }
                        var hoSo = _objectMapper.Map<HoSo>(model);
                        hoSo.BenhNhanId = benhNhan.Id;
                        hoSo.LoaiHoSoId = model.LoaiHoSoId;
                        hoSo.SetData("BacSiGiamDinh", model.BacSiGiamDinh);
                        hoSo.SetData("NguyenNhanXayRa", model.NguyenNhanXayRa);

                        var hoSoId = await _hosoRepository.InsertAndGetIdAsync(hoSo);

                        string contentName = CreateFileDataHoSo(hoSoId, model.ContentFileName);
                        hoSo.ContentFileName = contentName;
                        await _hosoRepository.UpdateAsync(hoSo);
                        await unitOfWork.CompleteAsync();
                        return Json(new
                        {
                            success = true,
                            type = "create",
                            redirectTo = Url.Action("TuThi", "HoSo"),
                            responseText = "success"
                        }, JsonRequestBehavior.AllowGet);
                    }
                }

            }
            catch (Exception e)
            {
                msg = e.Message;
            }

            return Json(new
            {
                success = false,
                responseText = msg
            }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region KhamTrinh

        [AbpMvcAuthorize(AppPermissions.Pages_HoSoKhamTrinh_Read)]
        public ActionResult KhamTrinh()
        {
            return View("KhamTrinh/KhamTrinh");
        }
        [AbpMvcAuthorize(AppPermissions.Pages_HoSoKhamTrinh_Create)]
        public ActionResult AddKhamTrinh()
        {
            int type = 2;
            var path = CreateFileTempAddHoSo(type);
            var model = new HoSoKhamTrinhDto
            {
                IsHaveContent = false,
                ContentFileName = path,
                NgayGiamDinh = DateTime.Now.Date,
                NgayQuyetDinhTrungCau = DateTime.Now.Date,
                NgayXayRa = DateTime.Now.Date,
                LoaiHoSoId = type,
                ThongTinBenhNhan = new BenhNhanDto
                {
                    Id = 0,
                    NamSinh = DateTime.Now.AddYears(-20).Date
                }
            };
            return PartialView("~/Areas/Admin/Views/HoSo/KhamTrinh/AddKhamTrinh.cshtml", model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_HoSoKhamTrinh_Update)]
        public ActionResult EditKhamTrinh(int id)
        {
            ViewBag.HoSoPath = HoSoDataPath;
            var hoSo = _hosoRepository.GetAll().FirstOrDefault(x => x.Id.Equals(id));
            var benhNhan = _benhnhanRepository.FirstOrDefault(x => x.Id.Equals(hoSo.BenhNhanId));
            var model = hoSo.GetKhamTrinhDto(benhNhan);
            ViewBag.NhanViens = hoSo.NhanViens.Select(x => new CheckBoxViewModel()
            {
                NhanVienId = x.Id,
                TenNhanVien = x.HoTen,
                TenChucVu = x.ChucVu.TenChucVu
            }).ToList();
            return PartialView("~/Areas/Admin/Views/HoSo/KhamTrinh/EditKhamTrinh.cshtml", model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_HoSoKhamTrinh_Update)]
        [HttpPost]
        [WrapResult(WrapOnSuccess = false, WrapOnError = true)]
        public async Task<JsonResult> EditKhamTrinh(HoSoKhamTrinhDto model)
        {

            var msg = string.Empty;
            try
            {
                if (ModelState.IsValid)
                {
                    using (var unitOfWork = UnitOfWorkManager.Begin())
                    {
                        var hoSo = await _hosoRepository.GetAll().Include(x => x.NhanViens)
                            .Where(x => x.Id == model.Id)
                            .FirstOrDefaultAsync();
                        if (hoSo != null)
                        {
                            var oldNhanViens = hoSo.NhanViens.ToList();
                            var imageAva = Request.Files;
                            if (imageAva.Count > 0)
                            {
                                var benhnhan = _benhnhanRepository.FirstOrDefault(x => x.Id == model.ThongTinBenhNhan.Id);
                                if (benhnhan != null)
                                {
                                    var thongtinBenhNhan = benhnhan.GetData<ThongTinCaNhan>("ThongTinCaNhan");
                                    if (thongtinBenhNhan != null)
                                    {
                                        var path = PathBenhNhanAvatar + thongtinBenhNhan.AvatarName;
                                        if (!string.IsNullOrEmpty(thongtinBenhNhan.AvatarName))
                                        {
                                            DirectoryHelpers.DeleteFile(Server.MapPath(path));
                                        }
                                        var newAva = string.Empty;
                                        newAva = imageAva.Count > 0 ? Path.GetFileNameWithoutExtension(imageAva[0].FileName) + "_" + Guid.NewGuid().ToString().Substring(0, 4)
                                                                    + Path.GetExtension(imageAva[0].FileName) : "";
                                        thongtinBenhNhan.AvatarName = newAva;
                                        benhnhan.SetData("ThongTinCaNhan", thongtinBenhNhan);
                                        var pathImageAva = Server.MapPath(PathBenhNhanAvatar);
                                        DirectoryHelpers.CreateDirectory(pathImageAva);
                                        imageAva[0].SaveAs(pathImageAva + newAva);
                                        CurrentUnitOfWork.SaveChanges();
                                    }
                                }
                            }

                            model.MapTo(hoSo);
                            hoSo.NhanViens = oldNhanViens;
                            // remove old nhanviens
                            foreach (var item in oldNhanViens.ToList())
                            {
                                hoSo.NhanViens.Remove(item);
                            }
                            // add new nhanviens
                            var nhanViens = await _nhanvienRepository.GetAll().ToListAsync();
                            if (model.NhanVienIds != null && model.NhanVienIds.Any())
                            {
                                foreach (var item in model.NhanVienIds)
                                {
                                    var nhanVien = nhanViens.FirstOrDefault(x => x.Id == item);
                                    if (nhanVien != null)
                                    {
                                        hoSo.NhanViens.Add(nhanVien);
                                    }
                                }
                            }

                            hoSo.SetData("BacSiGiamDinh", model.BacSiGiamDinh);
                            hoSo.SetData("NgayXayRa", model.NgayXayRa);
                            await _hosoRepository.UpdateAsync(hoSo);
                            if (model.HoSoImageDtos != null && model.HoSoImageDtos.Any())
                            {
                                foreach (var item in model.HoSoImageDtos)
                                {
                                    var image = await _hinhAnhRepository.FirstOrDefaultAsync(x => x.Id.Equals(item.Id));
                                    if (image != null)
                                    {
                                        image.DuocChon = item.DuocChon;
                                        image.ThuTuChon = item.ThuTuChon;
                                        await _hinhAnhRepository.UpdateAsync(image);
                                    }
                                }
                            }

                            await unitOfWork.CompleteAsync();
                            return Json(new
                            {
                                success = true,
                                type = "update",
                                redirectTo = Url.Action("KhamTrinh", "HoSo"),
                                responseText = "success"
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                msg = e.Message;
            }

            return Json(new
            {
                success = false,
                responseText = msg
            }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [AbpMvcAuthorize(AppPermissions.Pages_HoSoKhamTrinh_Create)]
        [WrapResult(WrapOnSuccess = false, WrapOnError = true)]
        public async Task<JsonResult> AddKhamTrinh(HoSoKhamTrinhDto model)
        {
            var msg = string.Empty;

            try
            {
                using (var unitOfWork = UnitOfWorkManager.Begin())
                {
                    if (ModelState.IsValid)
                    {
                        var image = Request.Files;
                        var benhNhan = _benhnhanRepository.GetAll()
                            .FirstOrDefault(x => x.Id == model.ThongTinBenhNhan.Id);
                        var avaName = string.Empty;
                        if (benhNhan == null)
                        {
                            benhNhan = _objectMapper.Map<BenhNhan>(model.ThongTinBenhNhan);

                            avaName = image.Count > 0 ? Path.GetFileNameWithoutExtension(image[0].FileName) + "_" + Guid.NewGuid().ToString().Substring(0, 4)
                                                        + Path.GetExtension(image[0].FileName) : "";
                            var thongTinCaNhan = new ThongTinCaNhan()
                            {
                                DanToc = model.ThongTinBenhNhan.ThongTinCaNhan.DanToc,
                                TonGiao = model.ThongTinBenhNhan.ThongTinCaNhan.TonGiao,
                                NgheNghiep = model.ThongTinBenhNhan.ThongTinCaNhan.NgheNghiep,
                                QuocTich = model.ThongTinBenhNhan.ThongTinCaNhan.QuocTich,
                                TrinhDoVanHoa = model.ThongTinBenhNhan.ThongTinCaNhan.TrinhDoVanHoa,
                                AvatarName = avaName
                            };
                            benhNhan.SetData("ThongTinCaNhan", thongTinCaNhan);
                            benhNhan.Id = await _benhnhanRepository.InsertAndGetIdAsync(benhNhan);
                            CurrentUnitOfWork.SaveChanges();
                        }
                        if (image.Count > 0)
                        {
                            var path = Server.MapPath(PathBenhNhanAvatar);
                            DirectoryHelpers.CreateDirectory(path);
                            image[0].SaveAs(path + avaName);
                        }
                        var hoSo = _objectMapper.Map<HoSo>(model);
                        hoSo.BenhNhanId = benhNhan.Id;
                        hoSo.LoaiHoSoId = model.LoaiHoSoId;
                        hoSo.SetData("BacSiGiamDinh", model.BacSiGiamDinh);
                        hoSo.SetData("NgayXayRa", model.NgayXayRa);

                        var hoSoId = await _hosoRepository.InsertAndGetIdAsync(hoSo);

                        string contentName = CreateFileDataHoSo(hoSoId, model.ContentFileName);
                        hoSo.ContentFileName = contentName;
                        await _hosoRepository.UpdateAsync(hoSo);
                        await unitOfWork.CompleteAsync();
                        return Json(new
                        {
                            success = true,
                            type = "create",
                            redirectTo = Url.Action("KhamTrinh", "HoSo"),
                            responseText = "success"
                        }, JsonRequestBehavior.AllowGet);
                    }
                }

            }
            catch (Exception e)
            {
                msg = e.Message;
            }

            return Json(new
            {
                success = false,
                responseText = msg
            }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region ThuongTat

        [AbpMvcAuthorize(AppPermissions.Pages_HoSoThuongTat_Read)]
        public ActionResult ThuongTat()
        {
            return View("ThuongTat/ThuongTat");
        }
        [AbpMvcAuthorize(AppPermissions.Pages_HoSoThuongTat_Create)]
        public ActionResult AddThuongTat()
        {
            int type = 3;
            var path = CreateFileTempAddHoSo(type);
            var model = new HoSoThuongTatDto
            {
                IsHaveContent = false,
                ContentFileName = path,
                NgayGiamDinh = DateTime.Now.Date,
                NgayQuyetDinhTrungCau = DateTime.Now.Date,
                NgayXayRa = DateTime.Now.Date,
                LoaiHoSoId = type,
                ThongTinBenhNhan = new BenhNhanDto
                {
                    Id = 0,
                    NamSinh = DateTime.Now.AddYears(-20).Date
                }
            };
            return PartialView("~/Areas/Admin/Views/HoSo/ThuongTat/AddThuongTat.cshtml", model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_HoSoThuongTat_Update)]
        public ActionResult EditThuongTat(int id)
        {
            ViewBag.HoSoPath = HoSoDataPath;
            var hoSo = _hosoRepository.GetAll().FirstOrDefault(x => x.Id.Equals(id));
            var benhNhan = _benhnhanRepository.FirstOrDefault(x => x.Id.Equals(hoSo.BenhNhanId));
            var model = hoSo.GetThuongTatDto(benhNhan);
            ViewBag.NhanViens = hoSo.NhanViens.Select(x => new CheckBoxViewModel()
            {
                NhanVienId = x.Id,
                TenNhanVien = x.HoTen,
                TenChucVu = x.ChucVu.TenChucVu
            }).ToList();
            return PartialView("~/Areas/Admin/Views/HoSo/ThuongTat/EditThuongTat.cshtml", model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_HoSoThuongTat_Update)]
        [HttpPost]
        [WrapResult(WrapOnSuccess = false, WrapOnError = true)]
        public async Task<JsonResult> EditThuongTat(HoSoThuongTatDto model)
        {

            var msg = string.Empty;
            try
            {
                if (ModelState.IsValid)
                {
                    using (var unitOfWork = UnitOfWorkManager.Begin())
                    {
                        var hoSo = await _hosoRepository.GetAll().Include(x => x.NhanViens)
                            .Where(x => x.Id == model.Id)
                            .FirstOrDefaultAsync();
                        if (hoSo != null)
                        {
                            var oldNhanViens = hoSo.NhanViens.ToList();
                            var imageAva = Request.Files;
                            if (imageAva.Count > 0)
                            {
                                var benhnhan = _benhnhanRepository.FirstOrDefault(x => x.Id == model.ThongTinBenhNhan.Id);
                                if (benhnhan != null)
                                {
                                    var thongtinBenhNhan = benhnhan.GetData<ThongTinCaNhan>("ThongTinCaNhan");
                                    if (thongtinBenhNhan != null)
                                    {
                                        var path = PathBenhNhanAvatar + thongtinBenhNhan.AvatarName;
                                        if (!string.IsNullOrEmpty(thongtinBenhNhan.AvatarName))
                                        {
                                            DirectoryHelpers.DeleteFile(Server.MapPath(path));
                                        }
                                        var newAva = string.Empty;
                                        newAva = imageAva.Count > 0 ? Path.GetFileNameWithoutExtension(imageAva[0].FileName) + "_" + Guid.NewGuid().ToString().Substring(0, 4)
                                                                    + Path.GetExtension(imageAva[0].FileName) : "";
                                        thongtinBenhNhan.AvatarName = newAva;
                                        benhnhan.SetData("ThongTinCaNhan", thongtinBenhNhan);
                                        var pathImageAva = Server.MapPath(PathBenhNhanAvatar);
                                        DirectoryHelpers.CreateDirectory(pathImageAva);
                                        imageAva[0].SaveAs(pathImageAva + newAva);
                                        CurrentUnitOfWork.SaveChanges();
                                    }
                                }
                            }

                            model.MapTo(hoSo);
                            hoSo.NhanViens = oldNhanViens;
                            // remove old nhanviens
                            foreach (var item in oldNhanViens.ToList())
                            {
                                hoSo.NhanViens.Remove(item);
                            }
                            // add new nhanviens
                            var nhanViens = await _nhanvienRepository.GetAll().ToListAsync();
                            if (model.NhanVienIds != null && model.NhanVienIds.Any())
                            {
                                foreach (var item in model.NhanVienIds)
                                {
                                    var nhanVien = nhanViens.FirstOrDefault(x => x.Id == item);
                                    if (nhanVien != null)
                                    {
                                        hoSo.NhanViens.Add(nhanVien);
                                    }
                                }
                            }

                            hoSo.SetData("BacSiGiamDinh", model.BacSiGiamDinh);
                            hoSo.SetData("NgayXayRa", model.NgayXayRa);
                            await _hosoRepository.UpdateAsync(hoSo);
                            if (model.HoSoImageDtos != null && model.HoSoImageDtos.Any())
                            {
                                foreach (var item in model.HoSoImageDtos)
                                {
                                    var image = await _hinhAnhRepository.FirstOrDefaultAsync(x => x.Id.Equals(item.Id));
                                    if (image != null)
                                    {
                                        image.DuocChon = item.DuocChon;
                                        image.ThuTuChon = item.ThuTuChon;
                                        await _hinhAnhRepository.UpdateAsync(image);
                                    }
                                }
                            }

                            await unitOfWork.CompleteAsync();
                            return Json(new
                            {
                                success = true,
                                type = "update",
                                redirectTo = Url.Action("ThuongTat", "HoSo"),
                                responseText = "success"
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                msg = e.Message;
            }

            return Json(new
            {
                success = false,
                responseText = msg
            }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [AbpMvcAuthorize(AppPermissions.Pages_HoSoThuongTat_Create)]
        [WrapResult(WrapOnSuccess = false, WrapOnError = true)]
        public async Task<JsonResult> AddThuongTat(HoSoThuongTatDto model)
        {
            var msg = string.Empty;

            try
            {
                using (var unitOfWork = UnitOfWorkManager.Begin())
                {
                    if (ModelState.IsValid)
                    {
                        var image = Request.Files;
                        var benhNhan = _benhnhanRepository.GetAll()
                            .FirstOrDefault(x => x.Id == model.ThongTinBenhNhan.Id);
                        var avaName = string.Empty;
                        if (benhNhan == null)
                        {
                            benhNhan = _objectMapper.Map<BenhNhan>(model.ThongTinBenhNhan);

                            avaName = image.Count > 0 ? Path.GetFileNameWithoutExtension(image[0].FileName) + "_" + Guid.NewGuid().ToString().Substring(0, 4)
                                                        + Path.GetExtension(image[0].FileName) : "";
                            var thongTinCaNhan = new ThongTinCaNhan()
                            {
                                DanToc = model.ThongTinBenhNhan.ThongTinCaNhan.DanToc,
                                TonGiao = model.ThongTinBenhNhan.ThongTinCaNhan.TonGiao,
                                NgheNghiep = model.ThongTinBenhNhan.ThongTinCaNhan.NgheNghiep,
                                QuocTich = model.ThongTinBenhNhan.ThongTinCaNhan.QuocTich,
                                TrinhDoVanHoa = model.ThongTinBenhNhan.ThongTinCaNhan.TrinhDoVanHoa,
                                AvatarName = avaName
                            };
                            benhNhan.SetData("ThongTinCaNhan", thongTinCaNhan);
                            benhNhan.Id = await _benhnhanRepository.InsertAndGetIdAsync(benhNhan);
                            CurrentUnitOfWork.SaveChanges();
                        }
                        if (image.Count > 0)
                        {
                            var path = Server.MapPath(PathBenhNhanAvatar);
                            DirectoryHelpers.CreateDirectory(path);
                            image[0].SaveAs(path + avaName);
                        }
                        var hoSo = _objectMapper.Map<HoSo>(model);
                        hoSo.BenhNhanId = benhNhan.Id;
                        hoSo.LoaiHoSoId = model.LoaiHoSoId;
                        hoSo.SetData("BacSiGiamDinh", model.BacSiGiamDinh);
                        hoSo.SetData("NgayXayRa", model.NgayXayRa);

                        var hoSoId = await _hosoRepository.InsertAndGetIdAsync(hoSo);

                        string contentName = CreateFileDataHoSo(hoSoId, model.ContentFileName);
                        hoSo.ContentFileName = contentName;
                        await _hosoRepository.UpdateAsync(hoSo);
                        await unitOfWork.CompleteAsync();
                        return Json(new
                        {
                            success = true,
                            type = "create",
                            redirectTo = Url.Action("ThuongTat", "HoSo"),
                            responseText = "success"
                        }, JsonRequestBehavior.AllowGet);
                    }
                }

            }
            catch (Exception e)
            {
                msg = e.Message;
            }

            return Json(new
            {
                success = false,
                responseText = msg
            }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Event

        [WrapResult(WrapOnSuccess = false, WrapOnError = true)]
        public async Task<ActionResult> BenhNhanList()
        {
            var benhnhan = await _benhnhanRepository.GetAll()
                .Select(x => new BenhNhanDto
                {
                    Id = x.Id,
                    HoTen = x.HoTen,
                    DiaChi = x.DiaChi,
                }).ToListAsync();

            return Json(benhnhan, JsonRequestBehavior.AllowGet);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_HoSoTuThi_Read)]
        [WrapResult(WrapOnSuccess = false, WrapOnError = true)]
        public async Task<ActionResult> GetHoSos([DataSourceRequest] DataSourceRequest request, int loaiHoSo)
        {
            var data = await _hosoRepository.GetAll()
                .Where(x => x.LoaiHoSoId.Equals(loaiHoSo))
                .Include(x => x.BenhNhan)
                .Include(x => x.LoaiHoSo)
                .Select(x => new HoSoTuThiDto
                {
                    Id = x.Id,
                    SoHoSo = x.SoHoSo,
                    SoQuyetDinhTcgd = x.SoQuyetDinhTcgd,
                    //TenLoaiHoSo = x.LoaiHoSo.TenLoaiHs,
                    ThongTinBenhNhan = new BenhNhanDto
                    {
                        HoTen = x.BenhNhan.HoTen,
                        DiaChi = x.BenhNhan.DiaChi,
                        GioiTinh = x.BenhNhan.GioiTinh,
                        NamSinh = x.BenhNhan.NamSinh
                    },
                    CoQuanTc = x.CoQuanTc
                })
                .ToListAsync();
            return Json(data.ToDataSourceResult(request));
        }

        [WrapResult(WrapOnSuccess = false, WrapOnError = true)]
        [AcceptVerbs(HttpVerbs.Post)]
        [AbpMvcAuthorize(AppPermissions.Pages_HoSoTuThi_Delete)]
        public async Task<ActionResult> Delete([DataSourceRequest] DataSourceRequest request, int id)
        {
            using (var uniOfWork = UnitOfWorkManager.Begin())
            {
                if (id != 0)
                {
                    var hoSo = await _hosoRepository.FirstOrDefaultAsync(x => x.Id == id);
                    if (hoSo != null)
                    {
                        DirectoryHelpers.CreateDirectory(Server.MapPath("~/App_Data/" + hoSo.Id));
                        await _hosoRepository.DeleteAsync(hoSo);
                        await uniOfWork.CompleteAsync();
                        return Json(new
                        {
                            success = true,
                            responseText = "success"
                        }, JsonRequestBehavior.AllowGet);
                    }
                }

                return Json(new
                {
                    success = false,
                    responseText = "fail"
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public async Task UploadImage(int hoSoId, HttpFileCollectionBase attachments)
        {
            var imageUploads = attachments.GetMultiple("imageUpload");
            if (imageUploads != null && imageUploads.Any() && imageUploads.Count > 0)
            {
                try
                {
                    string strFullPath = string.Format(PathHoSoImage, hoSoId);
                    DirectoryHelper.CreateIfNotExists(Server.MapPath(strFullPath));
                    foreach (var file in imageUploads)
                    {
                        var path = Path.Combine(Server.MapPath(strFullPath), file.FileName);
                        DirectoryHelpers.DeleteFile(path);
                        file.SaveAs(path);
                        var image = new HinhAnh
                        {
                            HoSoId = hoSoId,
                            TenHinhAnh = file.FileName
                        };
                        await _hinhAnhRepository.InsertAsync(image);
                    }
                }
                catch (Exception e)
                {

                }
            }
        }
        [WrapResult(WrapOnSuccess = false, WrapOnError = true)]
        [AcceptVerbs(HttpVerbs.Post)]
        [AbpMvcAuthorize(AppPermissions.Pages_HoSoTuThi_Delete)]
        public async Task<ActionResult> DeleteImage(int id)
        {
            using (var unitOfWork = UnitOfWorkManager.Begin())
            {
                var image = await _hinhAnhRepository.FirstOrDefaultAsync(x => x.Id.Equals(id));
                if (image != null)
                {
                    await _hinhAnhRepository.DeleteAsync(image);
                    string strFullPath = string.Format(PathHoSoImage, image.HoSoId);
                    var path = Path.Combine(Server.MapPath(strFullPath), image.TenHinhAnh);
                    DirectoryHelpers.DeleteFile(path);
                    await unitOfWork.CompleteAsync();
                    return Json(new
                    {
                        success = true,
                        responseText = "success"
                    }, JsonRequestBehavior.AllowGet);
                }
                return Json(new
                {
                    success = false,
                    responseText = "fail"
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        [DisableValidation]
        public ActionResult UploadImageAsyn(IEnumerable<HttpPostedFileBase> files, int id)
        {

            try
            {
                using (var unitOfWork = UnitOfWorkManager.Begin())
                {
                    string strFullPath = string.Format(PathHoSoImage, id);
                    DirectoryHelper.CreateIfNotExists(Server.MapPath(strFullPath));
                    foreach (var file in files)
                    {
                        var path = Path.Combine(Server.MapPath(strFullPath), file.FileName);
                        DirectoryHelpers.DeleteFile(path);
                        file.SaveAs(path);
                        var image = new HinhAnh
                        {
                            HoSoId = id,
                            TenHinhAnh = file.FileName
                        };
                        _hinhAnhRepository.InsertAsync(image);
                    }

                    unitOfWork.Complete();
                }
            }
            catch (Exception e)
            {

            }
            return Content("");
        }
        public ActionResult DeleteImageAsyn(string[] fileNames, int id)
        {
            // The parameter of the Remove action must be called "fileNames"

            if (fileNames != null && id > 0)
            {
                string strFullPath = string.Format(PathHoSoImage, id);
                using (var unitOfWork = UnitOfWorkManager.Begin())
                {
                    foreach (var fullName in fileNames)
                    {
                        var fileName = Path.GetFileName(fullName);
                        var physicalPath = Path.Combine(Server.MapPath(strFullPath), fileName);

                        // TODO: Verify user permissions

                        var image = _hinhAnhRepository.GetAll().Where(x => x.TenHinhAnh.Equals(fileName)).ToList();
                        if (image.Any())
                        {
                            foreach (var img in image)
                            {
                                _hinhAnhRepository.Delete(img);
                            }

                            var path = Path.Combine(Server.MapPath(strFullPath), fileName);
                            DirectoryHelpers.DeleteFile(path);
                        }
                        else
                        {
                            return Content("Fail! File not exist");
                        }
                    }
                    unitOfWork.Complete();
                }
            }

            // Return an empty string to signify success
            return Content("");
        }
        public async Task<FileResult> DownloadImage(int id)
        {
            var image = await _hinhAnhRepository.FirstOrDefaultAsync(x => x.Id.Equals(id));
            if (image != null)
            {
                string strFullPath = string.Format(PathHoSoImage, image.HoSoId);
                var path = Path.Combine(Server.MapPath(strFullPath), image.TenHinhAnh);
                return File(path, System.Net.Mime.MediaTypeNames.Application.Octet, image.TenHinhAnh);
            }
            return null;
        }
        [DisableValidation]
        public ActionResult DownLoadFileSelect(HoSoTuThiDto model)
        {
            using (var memoryStream = new MemoryStream())
            {
                string path = Server.MapPath(string.Format(PathHoSoImage, model.Id));
                if (model.HoSoImageDtos.Count(x => x.DuocChon) == 1)
                {
                    var imageName = model.HoSoImageDtos.FirstOrDefault(x => x.DuocChon).TenHinhAnh;
                    return File(path + imageName, System.Net.Mime.MediaTypeNames.Application.Octet, imageName);
                }

                using (var ziparchive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
                {

                    for (int i = 0; i < model.HoSoImageDtos.Count; i++)
                    {
                        if (model.HoSoImageDtos[i].DuocChon)
                        {
                            ziparchive.CreateEntryFromFile(path + model.HoSoImageDtos[i].TenHinhAnh, model.HoSoImageDtos[i].TenHinhAnh);
                        }
                    }
                }
                return File(memoryStream.ToArray(), "application/zip", "Attachments.zip");


            }
        }
        public async Task<ActionResult> DownloadFile(int id, string type = "html")
        {
            var hoSo = await _hosoRepository.FirstOrDefaultAsync(x => x.Id.Equals(id));
            if (hoSo != null)
            {
                var benhNhan = _benhnhanRepository.FirstOrDefault(x => x.Id.Equals(hoSo.BenhNhanId));

                switch (hoSo.LoaiHoSoId)
                {
                    case 1:
                        var modelTuThi = hoSo.GetTuThiDto(benhNhan);
                        return GenerateFile("~/Areas/Admin/Views/HoSo/TuThi/_viewPrint.cshtml", modelTuThi, type, string.Format(HoSoDataPath, hoSo.Id));
                    case 2:
                        var modelKhamTrinh = hoSo.GetKhamTrinhDto(benhNhan);
                        return GenerateFile("~/Areas/Admin/Views/HoSo/KhamTrinh/_viewPrint.cshtml", modelKhamTrinh, type, string.Format(HoSoDataPath, hoSo.Id));
                    case 3:
                        var modelThuongTat = hoSo.GetThuongTatDto(benhNhan);
                        return GenerateFile("~/Areas/Admin/Views/HoSo/ThuongTat/_viewPrint.cshtml", modelThuongTat, type, string.Format(HoSoDataPath, hoSo.Id));
                    default:
                        break;
                }
            }
            return null;
        }
        public string CreateFileTempAddHoSo(int typeHoSo)
        {
            string filePath;
            switch (typeHoSo)
            {
                case 1:
                    filePath = Server.MapPath(PathTemplateTuThi);//Template tử thi
                    break;
                case 2:
                    filePath = Server.MapPath(PathTemplateKhamTrinh);//Template khám trinh
                    break;
                case 3:
                    filePath = Server.MapPath(PathTemplateThuongTat);//Template thương tật
                    break;
                default:
                    filePath = Server.MapPath(PathTemplateTuThi);//Template tử thi
                    break;
            }
            
            string pathTemp = Server.MapPath(PathTemp);
            DirectoryHelpers.DeleteAllFiles(pathTemp, DateTime.Now.AddDays(-1));
            DirectoryHelpers.CreateDirectory(pathTemp);
            string fileName = Guid.NewGuid().ToString();
            string newpath = pathTemp + fileName + ".docx";//Path Save Data

            System.IO.File.Copy(filePath, newpath);
            return newpath;
        }

        public string CreateFileDataHoSo(int hoSoId, string pathTemp)
        {
            string newpath = Server.MapPath(string.Format(HoSoDataPath, hoSoId));
            DirectoryHelpers.CreateDirectory(newpath);
            string contentName = Guid.NewGuid().ToString();
            string newPathFile = newpath + "/" + contentName + ".docx";
            System.IO.File.Copy(pathTemp, newPathFile);
            DirectoryHelpers.DeleteFile(pathTemp);
            return contentName + ".docx";
        }

        public JsonResult AddTempPhuLuc(string temp, int id)
        {
            var msg = string.Empty;
            try
            {
                if (ModelState.IsValid)
                {
                    using (var unitOfWork = UnitOfWorkManager.Begin())
                    {
                        var hoSo = _hosoRepository.GetAll().FirstOrDefault(x => x.Id == id);
                        if (hoSo != null)
                        {
                            hoSo.PhuLucTemplate = temp;
                            _hosoRepository.Update(hoSo);
                            unitOfWork.Complete();
                            return Json(new
                            {
                                success = true,
                                type = "create",
                                responseText = "success"
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                msg = e.Message;
            }
            return Json(new
            {
                success = false,
                responseText = msg
            }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult AddTempHoSoDuongSu(string temp, int id)
        {
            var msg = string.Empty;
            try
            {
                if (ModelState.IsValid)
                {
                    using (var unitOfWork = UnitOfWorkManager.Begin())
                    {
                        var hoSo = _hosoRepository.GetAll().FirstOrDefault(x => x.Id == id);
                        if (hoSo != null)
                        {
                            hoSo.HoSoDuongSu = temp;
                            _hosoRepository.Update(hoSo);
                            unitOfWork.Complete();
                            return Json(new
                            {
                                success = true,
                                type = "create",
                                responseText = "success"
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                msg = e.Message;
            }
            return Json(new
            {
                success = false,
                responseText = msg
            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GenerateFile(string viewName, object model, string format, string path)
        {
            if (format == "html")
            {
                return View(viewName, model);
            }

            if (Request.Url != null)
            {
                string baseUrl = Request.Url.Authority;
                if (Request.ServerVariables["HTTPS"] == "on")
                {
                    baseUrl = "https://" + baseUrl;
                }
                else
                {
                    baseUrl = "http://" + baseUrl;
                }

                Helpers.ModifyInMemory.ActivateMemoryPatching();

                string html = RenderRazorViewToString(viewName, model);

                // To make the relative image paths work, base URL must be included in head section
                html = html.Replace("</head>", $"<base href='{baseUrl}'></base></head>");

                MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(html));
                Document doc = new Document(stream, new LoadOptions() { LoadFormat = LoadFormat.Html });
                string filePath = Server.MapPath(path);
                string fileName;
                try
                {
                    fileName = model.GetType().GetProperty("SoHoSo")?.GetValue(model, null).ToString().Normalize().Replace('/', '_') + "." + format;
                }
                catch
                {
                    fileName = Guid.NewGuid().ToString("N");
                }


                string pathFull = Path.Combine(filePath, fileName);
                doc.Save(pathFull);
                var cd = new System.Net.Mime.ContentDisposition
                {
                    FileName = Path.GetFileName(pathFull),
                    Inline = true
                };
                Response.AppendHeader("Content-Disposition", cd.ToString());
                byte[] filedata = System.IO.File.ReadAllBytes(pathFull);
                string contentType = MimeMapping.GetMimeMapping(pathFull);
                return File(filedata, contentType);
            }
            return null;
        }
        public string RenderRazorViewToString(string viewName, object model)
        {
            ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext,
                    viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View,
                    ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }

        public string RenderContentHoSo(HoSoTuThiDto hoSo)
        {
            Helpers.ModifyInMemory.ActivateMemoryPatching();
            string contentFileName = hoSo.ContentFileName;
            string filePath = Server.MapPath(string.Format(HoSoDataPath, hoSo.Id));
            if (!string.IsNullOrEmpty(contentFileName))
            {
                var path2 = Path.Combine(filePath, contentFileName);
                MemoryStream stream = new MemoryStream();
                Document doc = new Document(path2, new LoadOptions() { LoadFormat = LoadFormat.Docx });
                string html = doc.ToString(SaveFormat.Html);

                // Populate the html string here

                RegexOptions options = RegexOptions.IgnoreCase | RegexOptions.Singleline;
                Regex regx = new Regex("<body>(?<theBody>.*)</body>", options);

                Match match = regx.Match(html);

                if (match.Success)
                {
                    string theBody = match.Groups["theBody"].Value;
                    return theBody;
                }
            }

            return string.Empty;
        }
        #endregion
    }
}