﻿using Abp.Domain.Repositories;
using Abp.ObjectMapping;
using Abp.Web.Models;
using Abp.Web.Mvc.Authorization;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using TranLe.PhapY.Authorization;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using TranLe.PhapY.LoaiHoSos.Dto;
using TranLe.PhapY.Models;

namespace TranLe.PhapY.Web.Areas.Admin.Controllers
{
    public class LoaiHoSoController : PhapYControllerBase
    {
        // GET: LoaiHoSo
        private readonly IRepository<LoaiHoSo> _loaihosoRepository;
        private readonly IObjectMapper _objectMapper;

        // CONSTRUCTOR
        public LoaiHoSoController (
            IRepository<LoaiHoSo> loaihosoRepository,
            IObjectMapper objectMapper)
        {
            _loaihosoRepository = loaihosoRepository;
            _objectMapper = objectMapper;
        }

        // READ: LoaiHoSo
        [AbpMvcAuthorize(AppPermissions.Pages_LoaiHoSo_Read)]
        public ActionResult Index()
        {
            return View();
        }

        [AbpMvcAuthorize(AppPermissions.Pages_LoaiHoSo_Read)]
        [WrapResult(WrapOnSuccess = false, WrapOnError = true)]
        public async Task<ActionResult> GetLoaiHoSos([DataSourceRequest] DataSourceRequest request)
        {
            var data = await _loaihosoRepository.GetAll()
                .Select(x => new LoaiHoSoDto
                {
                    Id = x.Id,
                    TenLoaiHs = x.TenLoaiHs
                })
                .ToListAsync();
            return Json(data.ToDataSourceResult(request));
        }


        // CREATE: LoaiHoSo
        [AbpMvcAuthorize(AppPermissions.Pages_LoaiHoSo_Create)]
        public ActionResult Add()
        {
            var model = new LoaiHoSoDto { };
            return PartialView("~/Areas/Admin/Views/LoaiHoSo/Add.cshtml", model);
        }

        [HttpPost]
        [AbpMvcAuthorize(AppPermissions.Pages_LoaiHoSo_Create)]
        [WrapResult(WrapOnSuccess = false, WrapOnError = true)]
        public async Task<JsonResult> Add(LoaiHoSoDto model)
        {
            var msg = string.Empty;
            try
            {
                if (ModelState.IsValid)
                {
                    var loaiHoSo = _objectMapper.Map<LoaiHoSo>(model);
                    using (var unitOfWork = UnitOfWorkManager.Begin())
                    {
                        var loaiHoSoId = await _loaihosoRepository.InsertAndGetIdAsync(loaiHoSo);
                        await _loaihosoRepository.UpdateAsync(loaiHoSo);

                        await unitOfWork.CompleteAsync();
                        return Json(new
                        {
                            success = true,
                            type = "create",
                            responseText = "success"
                        }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                msg = e.Message;
            }
            return Json(new
            {
                success = false,
                responseText = msg
            }, JsonRequestBehavior.AllowGet);
        }


        // UPDATE: LoaiHoSo
        [AbpMvcAuthorize(AppPermissions.Pages_LoaiHoSo_Update)]
        public ActionResult Edit(int id)
        {
            var model = _loaihosoRepository.GetAll().Where(x => x.Id.Equals(id)).Select(x => new LoaiHoSoDto()
            {
                Id = x.Id,
                TenLoaiHs = x.TenLoaiHs
            }).FirstOrDefault();
            return PartialView("~/Areas/Admin/Views/LoaiHoSo/Add.cshtml", model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_LoaiHoSo_Update)]
        [HttpPost]
        [WrapResult(WrapOnSuccess = false, WrapOnError = true)]
        public async Task<JsonResult> Edit(LoaiHoSoDto model)
        {
            var msg = string.Empty;
            try
            {
                if (ModelState.IsValid)
                {
                    var loaiHoSo = _objectMapper.Map<LoaiHoSo>(model);
                    using (var unitOfWork = UnitOfWorkManager.Begin())
                    {
                        await _loaihosoRepository.UpdateAsync(loaiHoSo);

                        await unitOfWork.CompleteAsync();
                        return Json(new
                        {
                            success = true,
                            type = "update",
                            responseText = "success"
                        }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                msg = e.Message;
            }
            return Json(new
            {
                success = false,
                responseText = msg
            }, JsonRequestBehavior.AllowGet);
        }


        // DELETE: LoaiHoSo
        [WrapResult(WrapOnSuccess = false, WrapOnError = true)]
        [AcceptVerbs(HttpVerbs.Post)]
        [AbpMvcAuthorize(AppPermissions.Pages_LoaiHoSo_Delete)]
        public async Task<ActionResult> Delete([DataSourceRequest] DataSourceRequest request, int id)
        {
            using (var uniOfWork = UnitOfWorkManager.Begin())
            {
                if (id != 0)
                {
                    var loaiHoSo = await _loaihosoRepository.FirstOrDefaultAsync(x => x.Id == id);
                    if (loaiHoSo != null)
                    {
                        await _loaihosoRepository.UpdateAsync(loaiHoSo);
                        await _loaihosoRepository.DeleteAsync(loaiHoSo);
                        await uniOfWork.CompleteAsync();
                        return Json(new
                        {
                            success = true,
                            responseText = "success"
                        }, JsonRequestBehavior.AllowGet);
                    }
                }

                return Json(new
                {
                    success = false,
                    responseText = "fail"
                }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}