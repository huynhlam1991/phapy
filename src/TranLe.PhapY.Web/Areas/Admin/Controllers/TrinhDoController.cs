﻿using Abp.Domain.Repositories;
using Abp.ObjectMapping;
using Abp.Web.Models;
using Abp.Web.Mvc.Authorization;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using TranLe.PhapY.Authorization;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using TranLe.PhapY.Models;
using TranLe.PhapY.TrinhDos.Dto;

namespace TranLe.PhapY.Web.Areas.Admin.Controllers
{
    public class TrinhDoController : PhapYControllerBase
    {
        // GET: TrinhDo
        private readonly IRepository<TrinhDo> _trinhdoRepository;
        private readonly IObjectMapper _objectMapper;

        // CONSTRUCTOR
        public TrinhDoController (
            IRepository<TrinhDo> trinhdoRepository, 
            IObjectMapper objectMapper)
        {
            _trinhdoRepository = trinhdoRepository;
            _objectMapper = objectMapper;
        }


        // READ: TrinhDo
        [AbpMvcAuthorize(AppPermissions.Pages_TrinhDo_Read)]
        public ActionResult Index()
        {
            return View();
        }

        [AbpMvcAuthorize(AppPermissions.Pages_TrinhDo_Read)]
        [WrapResult(WrapOnSuccess = false, WrapOnError = true)]
        public async Task<ActionResult> GetTrinhDos ([DataSourceRequest] DataSourceRequest request)
        {
            var data = await _trinhdoRepository.GetAll()
                .Select(x => new TrinhDoDto
                {
                    Id = x.Id,
                    TenTrinhDo = x.TenTrinhDo,
                    DienGiai = x.DienGiai,
                })
                .ToListAsync();
            return Json(data.ToDataSourceResult(request));
        }


        // CREATE: TrinhDo
        [AbpMvcAuthorize(AppPermissions.Pages_TrinhDo_Create)]
        public ActionResult Add()
        {
            var model = new TrinhDoDto {};
            return PartialView("~/Areas/Admin/Views/TrinhDo/Add.cshtml", model);
        }

        [HttpPost]
        [AbpMvcAuthorize(AppPermissions.Pages_TrinhDo_Create)]
        [WrapResult(WrapOnSuccess = false, WrapOnError = true)]
        public async Task<JsonResult> Add(TrinhDoDto model)
        {
            var msg = string.Empty;
            try
            {
                if (ModelState.IsValid)
                {
                    var trinhDo = _objectMapper.Map<TrinhDo>(model);
                    using (var unitOfWork = UnitOfWorkManager.Begin())
                    {
                        var trinhDoId = await _trinhdoRepository.InsertAndGetIdAsync(trinhDo);
                        //nhanVien.MaNv = model.GenerateMaNv(nhanVienId.ToString());
                        //nhanVien.TaiKhoanId = model.UserId;
                        await _trinhdoRepository.UpdateAsync(trinhDo);

                        await unitOfWork.CompleteAsync();
                        return Json(new
                        {
                            success = true,
                            type = "create",
                            responseText = "success"
                        }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                msg = e.Message;
            }
            return Json(new
            {
                success = false,
                responseText = msg
            }, JsonRequestBehavior.AllowGet);
        }


        // UPDATE: TrinhDo
        [AbpMvcAuthorize(AppPermissions.Pages_TrinhDo_Update)]
        public ActionResult Edit(int id)
        {
            var model = _trinhdoRepository.GetAll().Where(x => x.Id.Equals(id)).Select(x => new TrinhDoDto()
            {
                Id = x.Id,
                TenTrinhDo = x.TenTrinhDo,
                DienGiai = x.DienGiai,
            }).FirstOrDefault();
            return PartialView("~/Areas/Admin/Views/TrinhDo/Add.cshtml", model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_TrinhDo_Update)]
        [HttpPost]
        [WrapResult(WrapOnSuccess = false, WrapOnError = true)]
        public async Task<JsonResult> Edit(TrinhDoDto model)
        {
            var msg = string.Empty;
            try
            {
                if (ModelState.IsValid)
                {
                    var trinhDo = _objectMapper.Map<TrinhDo>(model);
                    using (var unitOfWork = UnitOfWorkManager.Begin())
                    {
                        await _trinhdoRepository.UpdateAsync(trinhDo);

                        await unitOfWork.CompleteAsync();
                        return Json(new
                        {
                            success = true,
                            type = "update",
                            responseText = "success"
                        }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                msg = e.Message;
            }
            return Json(new
            {
                success = false,
                responseText = msg
            }, JsonRequestBehavior.AllowGet);
        }


        // DELETE: TrinhDo
        [WrapResult(WrapOnSuccess = false, WrapOnError = true)]
        [AcceptVerbs(HttpVerbs.Post)]
        [AbpMvcAuthorize(AppPermissions.Pages_TrinhDo_Delete)]
        public async Task<ActionResult> Delete([DataSourceRequest] DataSourceRequest request, int id)
        {
            using (var uniOfWork = UnitOfWorkManager.Begin())
            {
                if (id != 0)
                {
                    var trinhDo = await _trinhdoRepository.FirstOrDefaultAsync(x => x.Id == id);
                    if (trinhDo != null)
                    {
                        await _trinhdoRepository.UpdateAsync(trinhDo);
                        await _trinhdoRepository.DeleteAsync(trinhDo);
                        await uniOfWork.CompleteAsync();
                        return Json(new
                        {
                            success = true,
                            responseText = "success"
                        }, JsonRequestBehavior.AllowGet);
                    }
                }

                return Json(new
                {
                    success = false,
                    responseText = "fail"
                }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}