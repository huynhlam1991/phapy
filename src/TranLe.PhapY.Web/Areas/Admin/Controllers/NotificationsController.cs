﻿using System.Threading.Tasks;
using System.Web.Mvc;
using Abp.Web.Mvc.Authorization;
using TranLe.PhapY.Authorization;
using TranLe.PhapY.Notifications;
using TranLe.PhapY.Web.Controllers;

namespace TranLe.PhapY.Web.Areas.Admin.Controllers
{
    [AbpMvcAuthorize]
    public class NotificationsController : PhapYControllerBase
    {
        private readonly INotificationAppService _notificationApp;

        public NotificationsController(INotificationAppService notificationApp)
        {
            _notificationApp = notificationApp;
        }

        public ActionResult Index()
        {
            return View();
        }

        public async Task<PartialViewResult> SettingsModal()
        {
            var notificationSettings = await _notificationApp.GetNotificationSettings();
            return PartialView("_SettingsModal", notificationSettings);
        }
    }
}