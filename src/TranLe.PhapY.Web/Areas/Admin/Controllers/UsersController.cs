﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Abp.Application.Services.Dto;
using Abp.Web.Mvc.Authorization;
using TranLe.PhapY.Authorization;
using TranLe.PhapY.Authorization.Permissions;
using TranLe.PhapY.Authorization.Users;
using TranLe.PhapY.Web.Areas.Admin.Models.Users;
using TranLe.PhapY.Web.Controllers;
using TranLe.PhapY.Authorization.Roles;
using TranLe.PhapY.Authorization.Roles.Dto;
using System.Collections.Generic;
using Abp.Web.Models;
using TranLe.PhapY.Models;
using Abp.Domain.Repositories;
using System.Data.Entity;
using TranLe.PhapY.Authorization.Users.Dto;

namespace TranLe.PhapY.Web.Areas.Admin.Controllers
{
    [AbpMvcAuthorize(AppPermissions.Pages_Administration_Users)]
    public class UsersController : PhapYControllerBase
    {
        private readonly IUserAppService _userAppService;
        private readonly UserManager _userManager;
        private readonly IUserLoginAppService _userLoginAppService;
        private readonly IRoleAppService _roleAppService;
        private readonly IPermissionAppService _permissionAppService;
        private readonly IRepository<NhanVien> _nhanvienRepository;

        public UsersController(
            IUserAppService userAppService,
            UserManager userManager,
            IUserLoginAppService userLoginAppService,
            IRoleAppService roleAppService, 
            IPermissionAppService permissionAppService,
            IRepository<NhanVien> nhanvienRepository)
        {
            _userAppService = userAppService;
            _userManager = userManager;
            _userLoginAppService = userLoginAppService;
            _roleAppService = roleAppService;
            _permissionAppService = permissionAppService;
            _nhanvienRepository = nhanvienRepository;
        }

        public async Task<ActionResult> Index()
        {
            var roles = new List<ComboboxItemDto>();
            var permissions = _permissionAppService.GetAllPermissions()
                                                   .Items
                                                   .Select(p => new ComboboxItemDto(p.Name, new string('-', p.Level * 2) + " " + p.DisplayName))
                                                   .ToList();

            if (IsGranted(AppPermissions.Pages_Administration_Roles))
            {
                var getRolesOutput = await _roleAppService.GetRoles(new GetRolesInput());
                roles = getRolesOutput.Items.Select(r => new ComboboxItemDto(r.Id.ToString(), r.DisplayName)).ToList();
            }

            roles.Insert(0, new ComboboxItemDto("", ""));
            permissions.Insert(0, new ComboboxItemDto("", ""));

            var model = new UsersViewModel
            {
                FilterText = Request.QueryString["filterText"],
                Roles = roles,
                Permissions = permissions
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Administration_Users_Create, AppPermissions.Pages_Administration_Users_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(long? id)
        {
            var output = await _userAppService.GetUserForEdit(new NullableIdDto<long> { Id = id });
            var viewModel = new CreateOrEditUserModalViewModel(output);

            return PartialView("_CreateOrEditModal", viewModel);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Administration_Users_ChangePermissions)]
        public async Task<PartialViewResult> PermissionsModal(long id)
        {
            var user = await _userManager.GetUserByIdAsync(id);
            var output = await _userAppService.GetUserPermissionsForEdit(new EntityDto<long>(id));
            var viewModel = new UserPermissionsEditViewModel(output, user);

            return PartialView("_PermissionsModal", viewModel);
        }

        public async Task<PartialViewResult> LoginAttemptsModal()
        {
            var output = await _userLoginAppService.GetRecentUserLoginAttempts();
            var model = new UserLoginAttemptModalViewModel
            {
                LoginAttempts = output.Items.ToList()
            };
            return PartialView("_LoginAttemptsModal", model);
        }
        [WrapResult(WrapOnSuccess = false, WrapOnError = true)]
        public async Task<ActionResult> AccountLogin_Read(long? userId)
        {
            var nhanVien = await _nhanvienRepository.GetAll()
                .Where(x => x.TaiKhoanId.HasValue)
                .Select(x => x.TaiKhoanId).ToListAsync();
            var query = UserManager.Users
                .Where(x => x.UserName != "admin");
            query = userId == 0 ? query.Where(x => !nhanVien.Contains(x.Id)) : query.Where(x => !nhanVien.Contains(x.Id) || x.Id == userId);

            var accList = await query.Select(x => new UserListDto
            {
                Id = x.Id,
                UserName = x.UserName
            })
            .ToListAsync();

            return Json(accList, JsonRequestBehavior.AllowGet);
        }
    }
}