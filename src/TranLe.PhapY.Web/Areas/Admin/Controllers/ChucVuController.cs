﻿using Abp.Domain.Repositories;
using Abp.ObjectMapping;
using Abp.Web.Models;
using Abp.Web.Mvc.Authorization;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using TranLe.PhapY.Authorization;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using TranLe.PhapY.ChucVus.Dto;
using TranLe.PhapY.Models;

namespace TranLe.PhapY.Web.Areas.Admin.Controllers
{
    public class ChucVuController : PhapYControllerBase
    {
        // GET: ChucVu
        private readonly IRepository<ChucVu> _chucvuRepository;
        private readonly IObjectMapper _objectMapper;

        // CONSTRUCTOR
        public ChucVuController (
            IRepository<ChucVu> chucvuRepository,
            IObjectMapper objectMapper)
        {
            _chucvuRepository = chucvuRepository;
            _objectMapper = objectMapper;
        }


        // READ: ChucVu
        [AbpMvcAuthorize(AppPermissions.Pages_ChucVu_Read)]
        public ActionResult Index()
        {
            return View();
        }

        [AbpMvcAuthorize(AppPermissions.Pages_ChucVu_Read)]
        [WrapResult(WrapOnSuccess = false, WrapOnError = true)]
        public async Task<ActionResult> GetChucVus ([DataSourceRequest] DataSourceRequest request)
        {
            var data = await _chucvuRepository.GetAll()
                .Select(x => new ChucVuDto
                {
                    Id = x.Id,
                    TenChucVu = x.TenChucVu,
                    DienGiai = x.DienGiai,
                }).ToListAsync();
            return Json(data.ToDataSourceResult(request));
        }


        // CREATE: ChucVu
        [AbpMvcAuthorize(AppPermissions.Pages_ChucVu_Create)]
        public ActionResult Add()
        {
            var model = new ChucVuDto { };
            return PartialView("~/Areas/Admin/Views/ChucVu/Add.cshtml", model);
        }

        [HttpPost]
        [AbpMvcAuthorize(AppPermissions.Pages_ChucVu_Create)]
        [WrapResult(WrapOnSuccess = false, WrapOnError = true)]
        public async Task<JsonResult> Add(ChucVuDto model)
        {
            var msg = string.Empty;
            try
            {
                if (ModelState.IsValid)
                {
                    var chucVu = _objectMapper.Map<ChucVu>(model);
                    using (var unitOfWork = UnitOfWorkManager.Begin())
                    {
                        var chucVuId = await _chucvuRepository.InsertAndGetIdAsync(chucVu);
                        //nhanVien.MaNv = model.GenerateMaNv(nhanVienId.ToString());
                        //nhanVien.TaiKhoanId = model.UserId;
                        await _chucvuRepository.UpdateAsync(chucVu);

                        await unitOfWork.CompleteAsync();
                        return Json(new
                        {
                            success = true,
                            type = "create",
                            responseText = "success"
                        }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                msg = e.Message;
            }
            return Json(new
            {
                success = false,
                responseText = msg
            }, JsonRequestBehavior.AllowGet);
        }


        // UPDATE: ChucVu
        [AbpMvcAuthorize(AppPermissions.Pages_ChucVu_Update)]
        public ActionResult Edit(int id)
        {
            var model = _chucvuRepository.GetAll().Where(x => x.Id.Equals(id)).Select(x => new ChucVuDto()
            {
                Id = x.Id,
                TenChucVu = x.TenChucVu,
                DienGiai = x.DienGiai,
            }).FirstOrDefault();
            return PartialView("~/Areas/Admin/Views/ChucVu/Add.cshtml", model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_ChucVu_Update)]
        [HttpPost]
        [WrapResult(WrapOnSuccess = false, WrapOnError = true)]
        public async Task<JsonResult> Edit(ChucVuDto model)
        {
            var msg = string.Empty;
            try
            {
                if (ModelState.IsValid)
                {
                    var chucVu = _objectMapper.Map<ChucVu>(model);
                    using (var unitOfWork = UnitOfWorkManager.Begin())
                    {
                        await _chucvuRepository.UpdateAsync(chucVu);

                        await unitOfWork.CompleteAsync();
                        return Json(new
                        {
                            success = true,
                            type = "update",
                            responseText = "success"
                        }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                msg = e.Message;
            }
            return Json(new
            {
                success = false,
                responseText = msg
            }, JsonRequestBehavior.AllowGet);
        }


        // DELETE: ChucVu
        [WrapResult(WrapOnSuccess = false, WrapOnError = true)]
        [AcceptVerbs(HttpVerbs.Post)]
        [AbpMvcAuthorize(AppPermissions.Pages_ChucVu_Delete)]
        public async Task<ActionResult> Delete([DataSourceRequest] DataSourceRequest request, int id)
        {
            using (var uniOfWork = UnitOfWorkManager.Begin())
            {
                if (id != 0)
                {
                    var chucVu = await _chucvuRepository.FirstOrDefaultAsync(x => x.Id == id);
                    if (chucVu != null)
                    {
                        await _chucvuRepository.UpdateAsync(chucVu);
                        await _chucvuRepository.DeleteAsync(chucVu);
                        await uniOfWork.CompleteAsync();
                        return Json(new
                        {
                            success = true,
                            responseText = "success"
                        }, JsonRequestBehavior.AllowGet);
                    }
                }

                return Json(new
                {
                    success = false,
                    responseText = "fail"
                }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}