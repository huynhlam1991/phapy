﻿using System.Web.Mvc;
using Abp.Web.Mvc.Authorization;
using TranLe.PhapY.Web.Areas.Admin.Models.Common.Modals;
using TranLe.PhapY.Web.Controllers;

namespace TranLe.PhapY.Web.Areas.Admin.Controllers
{
    [AbpMvcAuthorize]
    public class CommonController : PhapYControllerBase
    {
        public PartialViewResult LookupModal(LookupModalViewModel model)
        {
            return PartialView("Modals/_LookupModal", model);
        }
    }
}