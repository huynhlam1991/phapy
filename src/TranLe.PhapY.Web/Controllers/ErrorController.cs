﻿using System.Web.Mvc;
using Abp.Auditing;

namespace TranLe.PhapY.Web.Controllers
{
    public class ErrorController : PhapYControllerBase
    {
        [DisableAuditing]
        public ActionResult E404()
        {
            return View();
        }
    }
}