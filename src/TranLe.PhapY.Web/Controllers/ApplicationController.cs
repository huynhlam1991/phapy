﻿using System.Web.Mvc;
using Abp.Auditing;
using Abp.Web.Mvc.Authorization;

namespace TranLe.PhapY.Web.Controllers
{
    [AbpMvcAuthorize]
    public class ApplicationController : PhapYControllerBase
    {
        [DisableAuditing]
        public ActionResult Index()
        {
            /* Enable next line to redirect to Multi-Page Application */
            /* return RedirectToAction("Index", "Home", new {area = "Admin"}); */

            return RedirectToAction("Index", "Home", new {area = "Admin"});
        }
    }
}