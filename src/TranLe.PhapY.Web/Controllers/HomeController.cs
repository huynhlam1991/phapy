﻿using Abp.Web.Mvc.Authorization;
using System.Web.Mvc;

namespace TranLe.PhapY.Web.Controllers
{
    public class HomeController : PhapYControllerBase
    {
        [AbpMvcAuthorize]
        public ActionResult Index()
        {
            return RedirectToAction("Index", "Home", new { area = "Admin" });
        }
	}
}