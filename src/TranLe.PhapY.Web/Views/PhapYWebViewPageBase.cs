﻿using Abp.Dependency;
using Abp.Runtime.Session;
using Abp.Web.Mvc.Views;

namespace TranLe.PhapY.Web.Views
{
    public abstract class PhapYWebViewPageBase : PhapYWebViewPageBase<dynamic>
    {
       
    }

    public abstract class PhapYWebViewPageBase<TModel> : AbpWebViewPage<TModel>
    {
        public IAbpSession AbpSession { get; private set; }
        
        protected PhapYWebViewPageBase()
        {
            AbpSession = IocManager.Instance.Resolve<IAbpSession>();
            LocalizationSourceName = PhapYConsts.LocalizationSourceName;
        }
    }
}