﻿using System.Threading.Tasks;
using TranLe.PhapY.Sessions.Dto;

namespace TranLe.PhapY.Web.Session
{
    public interface IPerRequestSessionCache
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformationsAsync();
    }
}
