namespace TranLe.PhapY.Web.Navigation
{
    public static class PageNames
    {
        public static class App
        {
            public static class Common
            {
                public const string Administration = "Administration";
                public const string Roles = "Administration.Roles";
                public const string Users = "Administration.Users";
                public const string AuditLogs = "Administration.AuditLogs";
                public const string OrganizationUnits = "Administration.OrganizationUnits";
                public const string Languages = "Administration.Languages";
            }

            public static class Host
            {
                public const string Tenants = "Tenants";
                public const string Editions = "Editions";
                public const string Maintenance = "Administration.Maintenance";
                public const string Settings = "Administration.Settings.Host";
            }

            public static class Tenant
            {
                public const string Dashboard = "Dashboard.Tenant";
                public const string Settings = "Administration.Settings.Tenant";
                public const string NhanViens = "NhanViens.Tenant";
                public const string HoSos = "HoSos.Tenant";
                public const string HoSoTuThis = "HoSoTuThis.Tenant";
                public const string HoSoKhamTrinhs = "HoSoKhamTrinhs.Tenant";
                public const string HoSoThuongTats = "HoSoThuongTats.Tenant";
                public const string BenhNhans = "BenhNhans.Tenant";
                public const string TrinhDos = "TrinhDos.Tenant";
                public const string ChucVus = "ChucVus.Tenant";
                public const string LoaiHoSos = "LoaiHoSos.Tenant";
                public const string HoSoTuThi = "HoSoTuThi.Tenant";
            }
        }

        public static class Frontend
        {
            public const string Home = "Frontend.Home";
            public const string About = "Frontend.About";
        }
    }
}