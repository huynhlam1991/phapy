using System.ComponentModel.DataAnnotations;

namespace TranLe.PhapY.Web.Models.Account
{
    public class ImpersonateModel
    {
        public int? TenantId { get; set; }

        [Range(1, long.MaxValue)]
        public long UserId { get; set; }
    }
}