﻿using System;

namespace TranLe.PhapY.Web.Models.Layout
{
    public class TenantCustomCssViewModel
    {
        public Guid? CustomCssId { get; set; }
    }
}