﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace TranLe.PhapY.Web.Helpers
{
    public static class DirectoryHelpers
    {
        public static bool CreateDirectory(string path)
        {
            try
            {
                // Determine whether the directory exists.
                if (!Directory.Exists(path))
                {
                    // Try to create the directory.
                    Directory.CreateDirectory(path);
                    return true;
                }
            }
            catch
            {
                return false;
            }
            return true;
        }
        public static bool DeleteFile(string path)
        {
            try
            {
                if (File.Exists(path))
                {
                    File.Delete(path);
                    return true;
                }
            }
            catch
            {
                return false;
            }
            return true;
        }
        public static bool DeleteAllFiles(string path)
        {
            try
            {
                DirectoryInfo di = new DirectoryInfo(path);

                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }
            }
            catch
            {
                return false;
            }
            return true;
        }
        public static bool DeleteAllFiles(string path, DateTime dateLimit)
        {
            try
            {
                DirectoryInfo di = new DirectoryInfo(path);

                foreach (FileInfo file in di.GetFiles().Where(x=>x.CreationTime <= dateLimit))
                {
                    file.Delete();
                }
            }
            catch
            {
                return false;
            }
            return true;
        }
    }
}