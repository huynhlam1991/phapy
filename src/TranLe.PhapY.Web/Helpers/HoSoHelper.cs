﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using Abp.AutoMapper;
using Abp.Domain.Entities;
using Abp.Domain.Repositories;
using Aspose.Words;
using Aspose.Words.Tables;
using Microsoft.Ajax.Utilities;
using TranLe.PhapY.BenhNhans.Dto;
using TranLe.PhapY.Models;
using TranLe.PhapY.HoSos.Dto;
using TranLe.PhapY.NhanViens.Dto;

namespace TranLe.PhapY.Web.Helpers
{
    public static class HoSoHelper
    {
        public static async Task UpdateImages (this HoSo hoSo, HoSoTuThiDto model, IRepository<HinhAnh> _hinhAnhRepository)
        {
            if (model.HoSoImageDtos != null && model.HoSoImageDtos.Any())
            {
                foreach (var item in model.HoSoImageDtos)
                {
                    var image = await _hinhAnhRepository.FirstOrDefaultAsync(x => x.Id.Equals(item.Id));
                    if (image != null)
                    {
                        image.DuocChon = item.DuocChon;
                        image.ThuTuChon = item.ThuTuChon;
                        await _hinhAnhRepository.UpdateAsync(image);
                    }
                }
            }
        }

        public static string FormatNgayThangNam(this DateTime date)
        {
            return " Ngày "+ date.Day + " Tháng " + date.Month + " Năm " + date.Year;
        }
        public static string FormatGioNgayThangNam(this DateTime date)
        {
            return " " + date.Hour + " giờ " + date.Minute + " phút," + " Ngày " + date.Day + " Tháng " + date.Month + " Năm " + date.Year;
        }

        public static Document SetCustomProperties(HoSo hoSo, string path)
        {
            var document = new Document(path);
            switch (hoSo.LoaiHoSoId)
            {
                case 1:
                    document.SetCustomPropertiesTuThi(hoSo);
                    break;
                case 2:
                    document.SetCustomPropertiesKhamTrinh(hoSo);
                    break;
                case 3:
                    document.SetCustomPropertiesThuongTat(hoSo);
                    break;
                default:
                    document.SetCustomPropertiesTuThi(hoSo);
                    break;
            }

            return document;
        }
        public static void SetCustomPropertiesTuThi(this Document document, HoSo hoSo)
        {
            var infoBenhNhan = hoSo.BenhNhan.GetData<ThongTinCaNhan>("ThongTinCaNhan") ?? new ThongTinCaNhan();

            SetDocumentProperty(document, "p_SoHoSo", hoSo.SoHoSo);
            SetDocumentProperty(document, "p_NgayTaoHoSo", hoSo.CreationTime.FormatNgayThangNam());
            SetDocumentProperty(document, "p_HoTen", hoSo.BenhNhan.HoTen);
            SetDocumentProperty(document, "p_NamSinh", hoSo.BenhNhan.NamSinh.Year);
            SetDocumentProperty(document, "p_GioiTinh", hoSo.BenhNhan.GioiTinh.Equals(true) ? "Nam" : "Nữ");
            SetDocumentProperty(document, "p_DiaChi", hoSo.BenhNhan.DiaChi);
            SetDocumentProperty(document, "p_TrinhDoVanHoa", infoBenhNhan.TrinhDoVanHoa);
            SetDocumentProperty(document, "p_DanToc", infoBenhNhan.DanToc);
            SetDocumentProperty(document, "p_TonGiao", infoBenhNhan.TonGiao);
            SetDocumentProperty(document, "p_NgheNghiep", infoBenhNhan.NgheNghiep);
            SetDocumentProperty(document, "p_XayRa", hoSo.GetData<string>("NguyenNhanXayRa"));
            SetDocumentProperty(document, "p_Tai", hoSo.DiaDiemXayRa);
            SetDocumentProperty(document, "p_SoQDTrungCau", hoSo.SoQuyetDinhTcgd);
            SetDocumentProperty(document, "p_NgayQDTrungCau", hoSo.NgayQuyetDinhTrungCau.FormatNgayThangNam());
            SetDocumentProperty(document, "p_CoQuanTrungCau", hoSo.CoQuanTc);
            SetDocumentProperty(document, "p_NhanVien", hoSo.SoHoSo);
            SetDocumentProperty(document, "p_NgayGiamDinh", hoSo.NgayGiamDinh);
            SetDocumentProperty(document, "p_DiaDiemGiamDinh", hoSo.DiaDiemGiamDinh);
        }
        public static void SetCustomPropertiesKhamTrinh(this Document document, HoSo hoSo)
        {
            var infoBenhNhan = hoSo.BenhNhan.GetData<ThongTinCaNhan>("ThongTinCaNhan") ?? new ThongTinCaNhan();

            SetDocumentProperty(document, "p_SoHoSo", hoSo.SoHoSo);
            SetDocumentProperty(document, "p_NgayTaoHoSo", hoSo.CreationTime.FormatNgayThangNam());
            SetDocumentProperty(document, "p_HoTen", hoSo.BenhNhan.HoTen);
            SetDocumentProperty(document, "p_NamSinh", hoSo.BenhNhan.NamSinh.Year);
            SetDocumentProperty(document, "p_GioiTinh", hoSo.BenhNhan.GioiTinh.Equals(true) ? "Nam" : "Nữ");
            SetDocumentProperty(document, "p_DiaChi", hoSo.BenhNhan.DiaChi);
            SetDocumentProperty(document, "p_TrinhDoVanHoa", infoBenhNhan.TrinhDoVanHoa);
            SetDocumentProperty(document, "p_DanToc", infoBenhNhan.DanToc);
            SetDocumentProperty(document, "p_TonGiao", infoBenhNhan.TonGiao);
            SetDocumentProperty(document, "p_NgheNghiep", infoBenhNhan.NgheNghiep);
            SetDocumentProperty(document, "p_XayRa", hoSo.GetData<string>("NguyenNhanXayRa"));
            SetDocumentProperty(document, "p_Tai", hoSo.DiaDiemXayRa);
            SetDocumentProperty(document, "p_SoQDTrungCau", hoSo.SoQuyetDinhTcgd);
            SetDocumentProperty(document, "p_NgayQDTrungCau", hoSo.NgayQuyetDinhTrungCau.FormatNgayThangNam());
            SetDocumentProperty(document, "p_CoQuanTrungCau", hoSo.CoQuanTc);
            SetDocumentProperty(document, "p_NhanVien", hoSo.SoHoSo);
            SetDocumentProperty(document, "p_NgayGiamDinh", hoSo.NgayGiamDinh);
            SetDocumentProperty(document, "p_DiaDiemGiamDinh", hoSo.DiaDiemGiamDinh);
        }
        public static void SetCustomPropertiesThuongTat(this Document document, HoSo hoSo)
        {
            var infoBenhNhan = hoSo.BenhNhan.GetData<ThongTinCaNhan>("ThongTinCaNhan") ?? new ThongTinCaNhan();

            SetDocumentProperty(document, "p_SoHoSo", hoSo.SoHoSo);
            SetDocumentProperty(document, "p_NgayTaoHoSo", hoSo.CreationTime.FormatNgayThangNam());
            SetDocumentProperty(document, "p_HoTen", hoSo.BenhNhan.HoTen);
            SetDocumentProperty(document, "p_NamSinh", hoSo.BenhNhan.NamSinh.Year);
            SetDocumentProperty(document, "p_GioiTinh", hoSo.BenhNhan.GioiTinh.Equals(true) ? "Nam" : "Nữ");
            SetDocumentProperty(document, "p_DiaChi", hoSo.BenhNhan.DiaChi);
            SetDocumentProperty(document, "p_TrinhDoVanHoa", infoBenhNhan.TrinhDoVanHoa);
            SetDocumentProperty(document, "p_DanToc", infoBenhNhan.DanToc);
            SetDocumentProperty(document, "p_TonGiao", infoBenhNhan.TonGiao);
            SetDocumentProperty(document, "p_NgheNghiep", infoBenhNhan.NgheNghiep);
            SetDocumentProperty(document, "p_XayRa", hoSo.GetData<string>("NguyenNhanXayRa"));
            SetDocumentProperty(document, "p_Tai", hoSo.DiaDiemXayRa);
            SetDocumentProperty(document, "p_SoQDTrungCau", hoSo.SoQuyetDinhTcgd);
            SetDocumentProperty(document, "p_NgayQDTrungCau", hoSo.NgayQuyetDinhTrungCau.FormatNgayThangNam());
            SetDocumentProperty(document, "p_CoQuanTrungCau", hoSo.CoQuanTc);
            SetDocumentProperty(document, "p_NhanVien", hoSo.SoHoSo);
            SetDocumentProperty(document, "p_NgayGiamDinh", hoSo.NgayGiamDinh);
            SetDocumentProperty(document, "p_DiaDiemGiamDinh", hoSo.DiaDiemGiamDinh);
        }
        public static void SetDocumentProperty(Document doc, string propertyName, object value)
        {
            // Get all the custom properties
            object customProperties = doc.CustomDocumentProperties;
            Type customPropertiesType = customProperties.GetType();

            // Retrieve the specific custom property item
            object customPropertyItem = customPropertiesType.InvokeMember("Item",
                BindingFlags.Default | BindingFlags.GetProperty, null, customProperties,
                new object[] { propertyName });
            Type propertyNameType = customPropertyItem.GetType();

            // Set the value of the specific custom property item
            propertyNameType.InvokeMember("Value", BindingFlags.Default | BindingFlags.SetProperty, null,
                customPropertyItem, new object[] { value });
        }
        public static Table AddTableToDocument(Document doc, HoSo hoSo)
        {
            Table table = null;
            DocumentBuilder builder = new DocumentBuilder(doc);
            table = (Table) doc.GetChild(NodeType.Table, 0, true);
            if (hoSo.NhanViens != null && hoSo.NhanViens.Any())
            {

                // We call this method to start building the table.
                table = builder.StartTable();
                
                foreach (var nhanVien in hoSo.NhanViens)
                {
                    builder.InsertCell();
                    builder.Write(nhanVien.HoTen);

                    // Build the second cell
                    builder.InsertCell();
                    builder.Write(nhanVien.ChucVu != null ? nhanVien.ChucVu.TenChucVu : "");
                    // Call the following method to end the row and start a new row.
                    builder.EndRow();
                }
                // Signal that we have finished building the table.
                builder.EndTable();
                doc.LastSection.Body.AppendChild(table);
            }

            return table;
        }
        public static void RemoveSectionBreaks(this Document doc)
        {
            // Loop through all sections starting from the section that precedes the last one 
            // And moving to the first section.
            for (int i = doc.Sections.Count - 2; i >= 0; i--)
            {
                // Copy the content of the current section to the beginning of the last section.
                doc.LastSection.PrependContent(doc.Sections[i]);
                // Remove the copied section.
                doc.Sections[i].Remove();
            }
        }
        
        public static HoSoTuThiDto GetTuThiDto(this HoSo hoSo, BenhNhan benhNhan)
        {
            string pathHoSoImage = ConfigurationManager.AppSettings["HoSoImagePath"];
            var model = new HoSoTuThiDto();
            var benhNhanModel = new BenhNhanDto();
            if (benhNhan != null)
            {
                benhNhanModel.Id = benhNhan.Id;
                benhNhanModel.HoTen = benhNhan.HoTen;
                benhNhanModel.GioiTinh = benhNhan.GioiTinh;
                benhNhanModel.NamSinh = benhNhan.NamSinh;
                benhNhanModel.SoDt = benhNhan.SoDt;
                benhNhanModel.ThongTinCaNhan = benhNhan.GetData<ThongTinCaNhan>("ThongTinCaNhan");
                benhNhanModel.DiaChi = benhNhan.DiaChi;
            }
            model.IsHaveContent = true;
            model.BacSiGiamDinh = hoSo.GetData<string>("BacSiGiamDinh");
            if (hoSo != null)
            {
                model.ContentFileName = hoSo.ContentFileName;
                model.Id = hoSo.Id;
                model.LoaiHoSoId = hoSo.LoaiHoSoId;
                model.NgayGiamDinh = hoSo.NgayGiamDinh;
                model.NgayQuyetDinhTrungCau = hoSo.NgayQuyetDinhTrungCau;
                model.NguyenNhanXayRa = hoSo.GetData<string>("NguyenNhanXayRa");
                model.SoHoSo = hoSo.SoHoSo;
                model.SoQuyetDinhTcgd = hoSo.SoQuyetDinhTcgd;
                model.CoQuanTc = hoSo.CoQuanTc;
                model.DiaDiemGiamDinh = hoSo.DiaDiemGiamDinh;
                model.DiaDiemXayRa = hoSo.DiaDiemXayRa;
                model.PhuLucTempate = hoSo.PhuLucTemplate;
                if (hoSo.NhanViens.Any())
                {
                    model.NhanVienDtos = hoSo.NhanViens.Select(x => new NhanVienDto
                    {
                        Id = x.Id,
                        HoTen = x.HoTen,
                        DiaChi = x.DiaChi,
                        BsChinh = x.BsChinh,
                        NameChucVu = x.ChucVuId.HasValue ? x.ChucVu.TenChucVu : string.Empty,
                        NgaySinh = x.NgaySinh,
                        SoDt = x.SoDt,
                        TrinhDoId = x.TrinhDoId,
                        UserName = x.TaiKhoanId.HasValue ? x.TaiKhoan.UserName : string.Empty,
                        UserId = x.TaiKhoanId.HasValue ? x.TaiKhoan.Id : 0,
                        ChucVuId = x.ChucVuId
                    }).ToList();
                }
                model.CreationTime = hoSo.CreationTime;
                model.HoSoImageDtos = hoSo.HinhAnhs.Select(x => new HoSoImageDto()
                {
                    Id = x.Id,
                    HoSoId = x.HoSoId,
                    ImagePath = pathHoSoImage,
                    TenHinhAnh = x.TenHinhAnh,
                    DuocChon = x.DuocChon,
                    ThuTuChon = x.ThuTuChon
                }).ToList();
            }
            model.ThongTinBenhNhan = benhNhanModel;
            return model;
        }
        public static HoSoKhamTrinhDto GetKhamTrinhDto(this HoSo hoSo, BenhNhan benhNhan)
        {
            string pathHoSoImage = ConfigurationManager.AppSettings["HoSoImagePath"];
            var model = new HoSoKhamTrinhDto();
            var benhNhanModel = new BenhNhanDto();
            if (benhNhan != null)
            {
                benhNhanModel.Id = benhNhan.Id;
                benhNhanModel.HoTen = benhNhan.HoTen;
                benhNhanModel.GioiTinh = benhNhan.GioiTinh;
                benhNhanModel.NamSinh = benhNhan.NamSinh;
                benhNhanModel.SoDt = benhNhan.SoDt;
                benhNhanModel.ThongTinCaNhan = benhNhan.GetData<ThongTinCaNhan>("ThongTinCaNhan");
                benhNhanModel.DiaChi = benhNhan.DiaChi;
            }
            model.IsHaveContent = true;
            model.BacSiGiamDinh = hoSo.GetData<string>("BacSiGiamDinh");
            if (hoSo != null)
            {
                model.ContentFileName = hoSo.ContentFileName;
                model.Id = hoSo.Id;
                model.LoaiHoSoId = hoSo.LoaiHoSoId;
                model.NgayGiamDinh = hoSo.NgayGiamDinh;
                model.NgayQuyetDinhTrungCau = hoSo.NgayQuyetDinhTrungCau;
                model.NgayXayRa = hoSo.GetData<DateTime>("NgayXayRa");
                model.SoHoSo = hoSo.SoHoSo;
                model.SoQuyetDinhTcgd = hoSo.SoQuyetDinhTcgd;
                model.CoQuanTc = hoSo.CoQuanTc;
                model.DiaDiemGiamDinh = hoSo.DiaDiemGiamDinh;
                model.DiaDiemXayRa = hoSo.DiaDiemXayRa;
                model.PhuLucTempate = hoSo.PhuLucTemplate;
                model.HoSoDuongSu = hoSo.HoSoDuongSu;
                if (hoSo.NhanViens.Any())
                {
                    model.NhanVienDtos = hoSo.NhanViens.Select(x => new NhanVienDto
                    {
                        Id = x.Id,
                        HoTen = x.HoTen,
                        DiaChi = x.DiaChi,
                        BsChinh = x.BsChinh,
                        NameChucVu = x.ChucVuId.HasValue ? x.ChucVu.TenChucVu : string.Empty,
                        NgaySinh = x.NgaySinh,
                        SoDt = x.SoDt,
                        TrinhDoId = x.TrinhDoId,
                        UserName = x.TaiKhoanId.HasValue ? x.TaiKhoan.UserName : string.Empty,
                        UserId = x.TaiKhoanId.HasValue ? x.TaiKhoan.Id : 0,
                        ChucVuId = x.ChucVuId
                    }).ToList();
                }
                model.CreationTime = hoSo.CreationTime;
                model.HoSoImageDtos = hoSo.HinhAnhs.Select(x => new HoSoImageDto()
                {
                    Id = x.Id,
                    HoSoId = x.HoSoId,
                    ImagePath = pathHoSoImage,
                    TenHinhAnh = x.TenHinhAnh,
                    DuocChon = x.DuocChon,
                    ThuTuChon = x.ThuTuChon
                }).ToList();
            }
            model.ThongTinBenhNhan = benhNhanModel;
            return model;
        }
        public static HoSoThuongTatDto GetThuongTatDto(this HoSo hoSo, BenhNhan benhNhan)
        {
            string pathHoSoImage = ConfigurationManager.AppSettings["HoSoImagePath"];
            var model = new HoSoThuongTatDto();
            var benhNhanModel = new BenhNhanDto();
            if (benhNhan != null)
            {
                benhNhanModel.Id = benhNhan.Id;
                benhNhanModel.HoTen = benhNhan.HoTen;
                benhNhanModel.GioiTinh = benhNhan.GioiTinh;
                benhNhanModel.NamSinh = benhNhan.NamSinh;
                benhNhanModel.SoDt = benhNhan.SoDt;
                benhNhanModel.ThongTinCaNhan = benhNhan.GetData<ThongTinCaNhan>("ThongTinCaNhan");
                benhNhanModel.DiaChi = benhNhan.DiaChi;
            }
            model.IsHaveContent = true;
            model.BacSiGiamDinh = hoSo.GetData<string>("BacSiGiamDinh");
            if (hoSo != null)
            {
                model.ContentFileName = hoSo.ContentFileName;
                model.Id = hoSo.Id;
                model.LoaiHoSoId = hoSo.LoaiHoSoId;
                model.NgayGiamDinh = hoSo.NgayGiamDinh;
                model.NgayQuyetDinhTrungCau = hoSo.NgayQuyetDinhTrungCau;
                model.NgayXayRa = hoSo.GetData<DateTime>("NgayXayRa");
                model.SoHoSo = hoSo.SoHoSo;
                model.SoQuyetDinhTcgd = hoSo.SoQuyetDinhTcgd;
                model.CoQuanTc = hoSo.CoQuanTc;
                model.DiaDiemGiamDinh = hoSo.DiaDiemGiamDinh;
                model.DiaDiemXayRa = hoSo.DiaDiemXayRa;
                model.PhuLucTempate = hoSo.PhuLucTemplate;
                model.HoSoDuongSu = hoSo.HoSoDuongSu;
                if (hoSo.NhanViens.Any())
                {
                    model.NhanVienDtos = hoSo.NhanViens.Select(x => new NhanVienDto
                    {
                        Id = x.Id,
                        HoTen = x.HoTen,
                        DiaChi = x.DiaChi,
                        BsChinh = x.BsChinh,
                        NameChucVu = x.ChucVuId.HasValue ? x.ChucVu.TenChucVu : string.Empty,
                        NgaySinh = x.NgaySinh,
                        SoDt = x.SoDt,
                        TrinhDoId = x.TrinhDoId,
                        UserName = x.TaiKhoanId.HasValue ? x.TaiKhoan.UserName : string.Empty,
                        UserId = x.TaiKhoanId.HasValue ? x.TaiKhoan.Id : 0,
                        ChucVuId = x.ChucVuId
                    }).ToList();
                }
                model.CreationTime = hoSo.CreationTime;
                model.HoSoImageDtos = hoSo.HinhAnhs.Select(x => new HoSoImageDto()
                {
                    Id = x.Id,
                    HoSoId = x.HoSoId,
                    ImagePath = pathHoSoImage,
                    TenHinhAnh = x.TenHinhAnh,
                    DuocChon = x.DuocChon,
                    ThuTuChon = x.ThuTuChon
                }).ToList();
            }
            model.ThongTinBenhNhan = benhNhanModel;
            return model;
        }
    }
}