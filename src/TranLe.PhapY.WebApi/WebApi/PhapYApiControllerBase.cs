using Abp.WebApi.Controllers;

namespace TranLe.PhapY.WebApi
{
    public abstract class PhapYApiControllerBase : AbpApiController
    {
        protected PhapYApiControllerBase()
        {
            LocalizationSourceName = PhapYConsts.LocalizationSourceName;
        }
    }
}