﻿using System.Data.Common;
using System.Data.Entity;
using Abp.Zero.EntityFramework;
using TranLe.PhapY.Authorization.Roles;
using TranLe.PhapY.Authorization.Users;
using TranLe.PhapY.Chat;
using TranLe.PhapY.Friendships;
using TranLe.PhapY.Models;
using TranLe.PhapY.MultiTenancy;
using TranLe.PhapY.Storage;

namespace TranLe.PhapY.EntityFramework
{
    /* Constructors of this DbContext is important and each one has it's own use case.
     * - Default constructor is used by EF tooling on design time.
     * - constructor(nameOrConnectionString) is used by ABP on runtime.
     * - constructor(existingConnection) is used by unit tests.
     * - constructor(existingConnection,contextOwnsConnection) can be used by ABP if DbContextEfTransactionStrategy is used.
     * See http://www.aspnetboilerplate.com/Pages/Documents/EntityFramework-Integration for more.
     */

    public class PhapYDbContext : AbpZeroDbContext<Tenant, Role, User>
    {
        /* Define an IDbSet for each entity of the application */

        public virtual IDbSet<BinaryObject> BinaryObjects { get; set; }

        public virtual IDbSet<Friendship> Friendships { get; set; }

        public virtual IDbSet<ChatMessage> ChatMessages { get; set; }

        public PhapYDbContext()
            : base("Default")
        {
            
        }
        public DbSet<BenhNhan> BenhNhans { get; set; }
        public DbSet<ChucVu> ChucVus { get; set; }
        public DbSet<HinhAnh> HinhAnhs { get; set; }
        public DbSet<HoSo> HoSos { get; set; }
        public DbSet<LoaiHoSo> LoaiHoSos { get; set; }
        public DbSet<NhanVien> NhanViens { get; set; }
        public DbSet<TrinhDo> TrinhDos { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<NhanVien>()
                .HasMany<HoSo>(s => s.Hosos)
                .WithMany(c => c.NhanViens)
                .Map(cs =>
                {
                    cs.MapLeftKey("NhanVienId");
                    cs.MapRightKey("HoSoId");
                    cs.ToTable("NhanVienHoSo");
                });
            modelBuilder.Entity<NhanVien>()
                .HasOptional(x => x.TaiKhoan)
                .WithMany()
                .HasForeignKey(x => x.TaiKhoanId);


        }

        public PhapYDbContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {

        }

        public PhapYDbContext(DbConnection existingConnection)
           : base(existingConnection, false)
        {

        }

        public PhapYDbContext(DbConnection existingConnection, bool contextOwnsConnection)
            : base(existingConnection, contextOwnsConnection)
        {

        }
    }
}
