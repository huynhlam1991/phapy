﻿using System.Collections.Generic;
using System.Linq;
using TranLe.PhapY.EntityFramework;
using TranLe.PhapY.Models;

namespace TranLe.PhapY.Migrations.Seed.Tenants
{
    public class DefaultLoaiHoSo
    {
        public static List<LoaiHoSo> InitialCountries { get; private set; }

        private readonly PhapYDbContext _context;

        static DefaultLoaiHoSo()
        {
            InitialCountries = new List<LoaiHoSo>
            {
                new LoaiHoSo { TenLoaiHs = "Hồ Sơ Tử Thi"},
                new LoaiHoSo { TenLoaiHs = "Hồ Sơ Khám Trinh"},
                new LoaiHoSo { TenLoaiHs = "Hồ Sơ Thương Tật"}
            };
        }

        public DefaultLoaiHoSo(PhapYDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateLoaiHoSos();
        }

        private void CreateLoaiHoSos()
        {
            foreach (var loaiHoSo in InitialCountries)
            {
                AddLoaiHoSoIfNotExists(loaiHoSo);
            }
        }

        private void AddLoaiHoSoIfNotExists(LoaiHoSo loaiHoSo)
        {
            if (_context.LoaiHoSos.Any(l => l.TenLoaiHs == loaiHoSo.TenLoaiHs))
            {
                return;
            }

            _context.LoaiHoSos.Add(loaiHoSo);

            _context.SaveChanges();
        }
    }
}
