namespace TranLe.PhapY.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class DeleteTablePhuLucHoSo : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PhuLucHoSo", "LoaiHoSoId", "dbo.LoaiHoSo");
            DropIndex("dbo.PhuLucHoSo", new[] { "LoaiHoSoId" });
            DropTable("dbo.PhuLucHoSo",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PhuLucHoSo_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.PhuLucHoSo",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 200),
                        LoaiHoSoId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PhuLucHoSo_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.PhuLucHoSo", "LoaiHoSoId");
            AddForeignKey("dbo.PhuLucHoSo", "LoaiHoSoId", "dbo.LoaiHoSo", "Id", cascadeDelete: true);
        }
    }
}
