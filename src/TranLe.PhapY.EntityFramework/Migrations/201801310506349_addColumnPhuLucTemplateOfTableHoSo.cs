namespace TranLe.PhapY.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addColumnPhuLucTemplateOfTableHoSo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.HoSo", "PhuLucTemplate", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.HoSo", "PhuLucTemplate");
        }
    }
}
