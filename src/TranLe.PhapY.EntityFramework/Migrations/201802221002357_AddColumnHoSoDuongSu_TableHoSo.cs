namespace TranLe.PhapY.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddColumnHoSoDuongSu_TableHoSo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.HoSo", "HoSoDuongSu", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.HoSo", "HoSoDuongSu");
        }
    }
}
