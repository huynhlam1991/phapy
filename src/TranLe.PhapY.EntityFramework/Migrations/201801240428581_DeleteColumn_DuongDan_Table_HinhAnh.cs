namespace TranLe.PhapY.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DeleteColumn_DuongDan_Table_HinhAnh : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.HinhAnh", "DuongDan");
        }
        
        public override void Down()
        {
            AddColumn("dbo.HinhAnh", "DuongDan", c => c.String(nullable: false, maxLength: 200));
        }
    }
}
