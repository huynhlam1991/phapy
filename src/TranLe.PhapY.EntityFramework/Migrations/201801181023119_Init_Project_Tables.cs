namespace TranLe.PhapY.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Init_Project_Tables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BenhNhan",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        HoTen = c.String(nullable: false, maxLength: 100),
                        GioiTinh = c.Boolean(nullable: false),
                        NamSinh = c.DateTime(nullable: false),
                        DiaChi = c.String(maxLength: 500),
                        SoDt = c.String(maxLength: 100),
                        ExtensionData = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_BenhNhan_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.HoSo",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DiaDiemXayRa = c.String(nullable: false, maxLength: 200),
                        SoHoSo = c.String(nullable: false, maxLength: 15),
                        SoQuyetDinhTcgd = c.String(nullable: false, maxLength: 5),
                        CoQuanTc = c.String(nullable: false, maxLength: 100),
                        NgayQuyetDinhTrungCau = c.DateTime(nullable: false),
                        NgayGiamDinh = c.DateTime(nullable: false),
                        DiaDiemGiamDinh = c.String(maxLength: 200),
                        ContentFilePath = c.String(nullable: false),
                        BenhNhanId = c.Int(nullable: false),
                        LoaiHoSoId = c.Int(nullable: false),
                        ExtensionData = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_HoSo_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BenhNhan", t => t.BenhNhanId, cascadeDelete: true)
                .ForeignKey("dbo.LoaiHoSo", t => t.LoaiHoSoId, cascadeDelete: true)
                .Index(t => t.BenhNhanId)
                .Index(t => t.LoaiHoSoId);
            
            CreateTable(
                "dbo.HinhAnh",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TenHinhAnh = c.String(nullable: false, maxLength: 100),
                        DuongDan = c.String(nullable: false, maxLength: 200),
                        ThuTuChon = c.Int(nullable: false),
                        DuocChon = c.Boolean(nullable: false),
                        HoSoId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_HinhAnh_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.HoSo", t => t.HoSoId, cascadeDelete: true)
                .Index(t => t.HoSoId);
            
            CreateTable(
                "dbo.LoaiHoSo",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TenLoaiHs = c.String(nullable: false, maxLength: 100),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_LoaiHoSo_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PhuLucHoSo",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 200),
                        LoaiHoSoId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PhuLucHoSo_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.LoaiHoSo", t => t.LoaiHoSoId, cascadeDelete: true)
                .Index(t => t.LoaiHoSoId);
            
            CreateTable(
                "dbo.NhanVien",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        HoTen = c.String(nullable: false, maxLength: 200),
                        NgaySinh = c.DateTime(nullable: false),
                        MaNv = c.String(maxLength: 6),
                        SoDt = c.String(maxLength: 100),
                        DiaChi = c.String(maxLength: 500),
                        Email = c.String(maxLength: 30),
                        BsChinh = c.Boolean(nullable: false),
                        TrinhDoId = c.Int(),
                        ChucVuId = c.Int(),
                        TaiKhoanId = c.Long(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_NhanVien_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ChucVu", t => t.ChucVuId)
                .ForeignKey("dbo.AbpUsers", t => t.TaiKhoanId)
                .ForeignKey("dbo.TrinhDo", t => t.TrinhDoId)
                .Index(t => t.TrinhDoId)
                .Index(t => t.ChucVuId)
                .Index(t => t.TaiKhoanId);
            
            CreateTable(
                "dbo.ChucVu",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TenChucVu = c.String(nullable: false, maxLength: 100),
                        DienGiai = c.String(maxLength: 200),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ChucVu_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TrinhDo",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TenTrinhDo = c.String(nullable: false, maxLength: 100),
                        DienGiai = c.String(maxLength: 200),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TrinhDo_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.NhanVienHoSo",
                c => new
                    {
                        NhanVienId = c.Int(nullable: false),
                        HoSoId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.NhanVienId, t.HoSoId })
                .ForeignKey("dbo.NhanVien", t => t.NhanVienId, cascadeDelete: true)
                .ForeignKey("dbo.HoSo", t => t.HoSoId, cascadeDelete: true)
                .Index(t => t.NhanVienId)
                .Index(t => t.HoSoId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.NhanVien", "TrinhDoId", "dbo.TrinhDo");
            DropForeignKey("dbo.NhanVien", "TaiKhoanId", "dbo.AbpUsers");
            DropForeignKey("dbo.NhanVienHoSo", "HoSoId", "dbo.HoSo");
            DropForeignKey("dbo.NhanVienHoSo", "NhanVienId", "dbo.NhanVien");
            DropForeignKey("dbo.NhanVien", "ChucVuId", "dbo.ChucVu");
            DropForeignKey("dbo.PhuLucHoSo", "LoaiHoSoId", "dbo.LoaiHoSo");
            DropForeignKey("dbo.HoSo", "LoaiHoSoId", "dbo.LoaiHoSo");
            DropForeignKey("dbo.HinhAnh", "HoSoId", "dbo.HoSo");
            DropForeignKey("dbo.HoSo", "BenhNhanId", "dbo.BenhNhan");
            DropIndex("dbo.NhanVienHoSo", new[] { "HoSoId" });
            DropIndex("dbo.NhanVienHoSo", new[] { "NhanVienId" });
            DropIndex("dbo.NhanVien", new[] { "TaiKhoanId" });
            DropIndex("dbo.NhanVien", new[] { "ChucVuId" });
            DropIndex("dbo.NhanVien", new[] { "TrinhDoId" });
            DropIndex("dbo.PhuLucHoSo", new[] { "LoaiHoSoId" });
            DropIndex("dbo.HinhAnh", new[] { "HoSoId" });
            DropIndex("dbo.HoSo", new[] { "LoaiHoSoId" });
            DropIndex("dbo.HoSo", new[] { "BenhNhanId" });
            DropTable("dbo.NhanVienHoSo");
            DropTable("dbo.TrinhDo",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TrinhDo_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.ChucVu",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ChucVu_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.NhanVien",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_NhanVien_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.PhuLucHoSo",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PhuLucHoSo_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.LoaiHoSo",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_LoaiHoSo_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.HinhAnh",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_HinhAnh_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.HoSo",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_HoSo_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.BenhNhan",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_BenhNhan_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
