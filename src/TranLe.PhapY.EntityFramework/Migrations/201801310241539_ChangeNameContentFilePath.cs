namespace TranLe.PhapY.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeNameContentFilePath : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.HoSo", "ContentFileName", c => c.String(nullable: false));
            DropColumn("dbo.HoSo", "ContentFilePath");
        }
        
        public override void Down()
        {
            AddColumn("dbo.HoSo", "ContentFilePath", c => c.String(nullable: false));
            DropColumn("dbo.HoSo", "ContentFileName");
        }
    }
}
