﻿using EntityFramework.DynamicFilters;
using TranLe.PhapY.EntityFramework;

namespace TranLe.PhapY.Tests.TestDatas
{
    public class TestDataBuilder
    {
        private readonly PhapYDbContext _context;
        private readonly int _tenantId;

        public TestDataBuilder(PhapYDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            _context.DisableAllFilters();

            new TestOrganizationUnitsBuilder(_context, _tenantId).Create();

            _context.SaveChanges();
        }
    }
}
